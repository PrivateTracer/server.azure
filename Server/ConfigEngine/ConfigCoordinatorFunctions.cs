using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Web.Config;
using PrivateTracer.Server.Azure.Components.Web.Settings;
using PrivateTracer.Server.Azure.Components.Web.TracingSetsEngine;
using PrivateTracer.Server.Azure.DevServer.Controllers;

namespace PrivateTracer.Server.Azure.ConfigCoordinator.AppFunction
{
    public static class ConfigCoordinatorFunctions
    {
        [FunctionName("TracingSet_Receive")]
        public static async Task Run([ServiceBusTrigger("TracingSetsQueue", Connection = "TracingSetsQueueConnectionString")]
            Message message, ILogger log)
        {
            var json = Encoding.UTF8.GetString(message.Body);
            var tracingSetMessage = JsonConvert.DeserializeObject<TracingSetMessage>(json);
            var handler = new TracingSetQueueReceiver(
                new TracingSetDbInsertCommand(new EnvironmentVariableDbConfig("Config")),
                new TracingSetBlobWriter(new EnvironmentVariableStorageAccountConfig("Config"))
            );
            await handler.Execute(tracingSetMessage);
        }

        [FunctionName("Config_Update")]
        public static async Task Run([TimerTrigger("0 */2 * * * *")] 
            TimerInfo timerInfo, ILogger log)
        {
            var dbConfig = new EnvironmentVariableDbConfig("Config");
            var handler = new ConfigUpdateWriter(
                new ConfigBuilder(
                    new TracingSetsConfigBuilder(
                        dbConfig,
                        new EnvironmentVariableDp3TConfig()
                        ),
                    new GetLatestRivmAdviceCommand(dbConfig, new StandardUtcDateTimeProvider())
                    ),
                new ConfigBlobWriter(new EnvironmentVariableStorageAccountConfig("Config"))
            );
            handler.Execute();
        }
    }
}

using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.Tracers;
using PrivateTracer.Server.Azure.Components.WebJobs;

namespace TracerReceiver
{
    public class TracerReceiverAppFunction :  AppFunctionBase
    {
        [FunctionName("tracer")]
        public async Task<IActionResult> PostTracer([HttpTrigger(AuthorizationLevel.Anonymous, "post")] HttpRequest req, ILogger log, ExecutionContext context)
        {
            SetConfig(context);

            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var tracerArgs = JsonConvert.DeserializeObject<TracerArgs>(requestBody);

            var handler = new HttpPostTracerCommandV2(
                new TracerQueueSendCommand(new ServiceBusAppSettings(Configuration, "ServiceBus:Tracers")), 
                new TracerValidator(
                    new TracerValidationAppSettings(Configuration), 
                    new TracerAuthorisationTokenLuhnModNValidator(new LuhnModNConfig()), 
                    new TracerKeyValidator(new TracerKeyValidatorAppSettings(Configuration))
                ));

            return await handler.Execute(tracerArgs);
        }

#if DEBUG
        //TODO Remove
        [FunctionName("logconfig")]
        public async Task<IActionResult> GetConfig([HttpTrigger(AuthorizationLevel.Function, "get")]
            HttpRequest req, ILogger log, ExecutionContext context)
        {
            LogConfig(log, context);
            return new OkResult();
        }
#endif
    }
}

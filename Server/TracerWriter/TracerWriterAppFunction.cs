using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.Tracers;
using PrivateTracer.Server.Azure.Components.WebJobs;

namespace TracerWriter
{
    public class TracerWriterAppFunction : AppFunctionBase
    {
        [FunctionName("TracerWriter")]
        public async Task Run([ServiceBusTrigger("Tracers", Connection="TracerQueueConnectionString")]Message message, ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var json = Encoding.UTF8.GetString(message.Body);
            var tracerArgs = JsonConvert.DeserializeObject<TracerArgs>(json);
            var writer = new TracerDbInsertCommand(new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main), new StandardUtcDateTimeProvider());
            await writer.Execute(tracerArgs);
        }
    }
}

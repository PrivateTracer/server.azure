using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.WebJobs;

namespace TracerAuthorisationWriter
{
    public class TracerAuthorisationWriterAppFunction : AppFunctionBase
    {
        [FunctionName("TracerAuthorisationWriter")]
        public async Task Run([ServiceBusTrigger("Authorisations", Connection = "TracerAuthorisationQueueConnectionString")]Message message, ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var json = Encoding.UTF8.GetString(message.Body);
            var args = JsonConvert.DeserializeObject<TracerAuthorisationArgs>(json);
            var writer = new TracerDbAuthoriseCommand(new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main));
            await writer.Execute(args);
        }
    }
}

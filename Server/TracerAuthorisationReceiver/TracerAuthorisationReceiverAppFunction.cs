using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.Tracers;
using PrivateTracer.Server.Azure.Components.WebJobs;

namespace TracerAuthorisationReceiver
{
    public class TracerAuthorisationReceiverAppFunction : AppFunctionBase
    {
        [FunctionName("TracerAuthorisationReceiver")]
        public async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req, ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            LogConfig(log, executionContext);
            var json = await new StreamReader(req.Body).ReadToEndAsync();
            var args = JsonConvert.DeserializeObject<TracerAuthorisationArgs>(json);
            var handler = new HttpPostTracerAuthoriseCommand(
                new TracerAuthorisationQueueSendCommand(new ServiceBusAppSettings(Configuration, "ServiceBus:TracerAuths")), 
                new TracerAuthorisationTokenLuhnModNValidator(new LuhnModNConfig())
                );

            return handler.Execute(args);
        }
    }
}

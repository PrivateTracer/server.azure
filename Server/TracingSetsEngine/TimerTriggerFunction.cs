using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;
using PrivateTracer.Server.Azure.Components.WebJobs;

namespace PrivateTracer.Server.Azure.TracingSetsEngine
{
    public class TimerTriggerFunction : AppFunctionBase
    {
        private const string Name = "CuckooFilterBatch_TimerTrigger";

        [FunctionName(Name)]
        public async Task Run([TimerTrigger("0 */1000 * * * *"/*, RunOnStartup = true*/)] TimerInfo timerInfo, ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var source = new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main);
            var job = new DbConfigAppSettings(Configuration, DatabaseConfigNames.TracingSetsEngine);
            await new BuildCuckooFiltersBatchJob(source, job, 
                    new StandardUtcDateTimeProvider(),
                    new TracingSetMessageQueueSender(new ServiceBusAppSettings(Configuration, "ServiceBus:TracingSets")))
                .Execute();
        }
    }
}

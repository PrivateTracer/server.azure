﻿using System;
using System.Linq;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Tracers;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;

namespace PrivateTracer.Server.Azure.Components.pt.Mapping
{
    public static class MappingStuff
    {
        public static TracingSetConfigEntity ToDbEntity(this TracingSetMessage message)
            => new TracingSetConfigEntity
            {
                Release = message.Created,
                Qualifier = message.CreatedQualifier,
                CuckooFilter = message.CuckooFilter
            };
        
        public static TracerEntity ToDbEntity(this TracerArgs args)
            => new TracerEntity
            {
                TracerKeys = args.Items.Select(ToDbEntity).ToArray(),
                AuthorisationToken = args.Token,
                //Created set at the point of writing to the DB
            };

        private static TracerKey ToDbEntity(TracerKeyArgs args)
            => new TracerKey
            {
                Seed= args.Seed,
                Epoch = args.Epoch
            };
    }
}

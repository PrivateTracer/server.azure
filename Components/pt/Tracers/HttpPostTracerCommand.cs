using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class HttpPostTracerCommand
    {
        private readonly ITracerWriter _TracerDbInsertCommand;
        private readonly ITracerValidator _TracerValidator;

        public HttpPostTracerCommand(ITracerWriter infectionDbInsertCommand, ITracerValidator tracerValidator)
        {
            _TracerDbInsertCommand = infectionDbInsertCommand;
            _TracerValidator = tracerValidator;
        }

        public IActionResult Execute(TracerArgs args)
        {
            if (!_TracerValidator.Validate(args))
                return new BadRequestResult();

            _TracerDbInsertCommand.Execute(args).GetAwaiter().GetResult();


            //NB not using CreatedResult cos you cannot get the items afterwards.
            return new StatusCodeResult(204);
        }
    }
}
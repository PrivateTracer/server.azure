using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public interface ITracerWriter
    {
        Task Execute(TracerArgs args);
    }
}
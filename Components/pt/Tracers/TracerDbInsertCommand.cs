using System;
using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.pt.Mapping;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerDbInsertCommand : ITracerWriter
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _DateTimeProvider;

        public TracerDbInsertCommand(IDbConfig config, IUtcDateTimeProvider dateTimeProvider)
        {
            _DbConfig = config;
            _DateTimeProvider = dateTimeProvider;
        }

        public async Task Execute(TracerArgs args)
        {
            using var client = new CosmosClientEx(_DbConfig);
            var e = args.ToDbEntity();
            e.Created = _DateTimeProvider.Now();
            await client.InsertAsync(e);
        }
    }
}

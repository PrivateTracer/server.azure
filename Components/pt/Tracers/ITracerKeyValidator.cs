﻿namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public interface ITracerKeyValidator
    {
        bool Valid(TracerKeyArgs value);
    }
}
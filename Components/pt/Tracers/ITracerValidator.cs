namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public interface ITracerValidator
    {
        bool Validate(TracerArgs args);
    }
}
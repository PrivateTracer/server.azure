using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerValidator : ITracerValidator
    {
        private readonly ITracerValidatorConfig _Config;
        private readonly ITracerAuthorisationTokenValidator _AuthorisationTokenValidator;
        private readonly ITracerKeyValidator _TracerKeyValidator;

        public TracerValidator(ITracerValidatorConfig config, ITracerAuthorisationTokenValidator authorisationTokenValidator, ITracerKeyValidator tracerKeyValidator)
        {
            _Config = config;
            _AuthorisationTokenValidator = authorisationTokenValidator;
            _TracerKeyValidator = tracerKeyValidator;
        }

        public bool Validate(TracerArgs args)
        {
            if (args == null)
                return false;

            if (!_AuthorisationTokenValidator.IsValid(args.Token))
                return false;

            if (_Config.TracerKeyCountMin > args.Items.Length
                || args.Items.Length > _Config.TracerKeyCountMax)
                return false;

            return args.Items.All(_TracerKeyValidator.Valid);
        }
    }
}
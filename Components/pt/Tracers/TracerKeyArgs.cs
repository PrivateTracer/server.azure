namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerKeyArgs
    {
        public string Seed { get; set; }
        public int Epoch { get; set; }
    }
}
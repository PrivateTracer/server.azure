using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerQueueSendCommand : QueueSendCommand<TracerArgs>, ITracerWriter
    {
        public TracerQueueSendCommand(IServiceBusConfig sbConfig) : base(sbConfig)
        {
        }
    }
}
namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public interface ITracerValidatorConfig
    {
        int TracerKeyCountMin { get; }
        int TracerKeyCountMax { get; }
    }
}
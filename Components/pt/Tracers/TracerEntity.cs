using System;
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerEntity : DatabaseEntity
    {
        public TracerKey[] TracerKeys { get; set; }
        public string AuthorisationToken { get; set; }

        //TODO public int Ttl { get; set; }

        /// <summary>
        /// False by default, authorized via CPS
        /// </summary>
        public bool Authorised { get; set; }

        public DateTime Created { get; set; }
    }
}
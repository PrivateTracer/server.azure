using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class HttpPostTracerCommandV2
    {
        private readonly ITracerWriter _TracerWriter;
        private readonly ITracerValidator _TracerValidator;

        public HttpPostTracerCommandV2(ITracerWriter tracerWriter, ITracerValidator tracerValidator)
        {
            _TracerWriter = tracerWriter;
            _TracerValidator = tracerValidator;
        }

        public async Task<IActionResult> Execute(TracerArgs args)
        {
            if (!_TracerValidator.Validate(args))
                return new BadRequestResult();

            await _TracerWriter.Execute(args);

            //NB not using CreatedResult cos you cannot get the items afterwards.
            return new OkResult(); 
        }
    }
}
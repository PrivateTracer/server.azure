﻿using System;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerKeyValidator : ITracerKeyValidator
    {
        private readonly ITracerKeyValidatorConfig _Config;

        public TracerKeyValidator(ITracerKeyValidatorConfig config)
        {
            _Config = config;
        }

        public bool Valid(TracerKeyArgs value)
        {
            if (value == null)
                return false;

            if (_Config.EpochMin > value.Epoch || value.Epoch > _Config.EpochMax)
                return false;

            if (string.IsNullOrEmpty(value.Seed))
                return false;

            //TODO how to accodomodate Android? Or just skip to the conversion?
            //if (value.Seed.Length != _Config.SeedBase64Length)
            //    return false;

            try
            {
                //GUARANTEES a successful conversion at the point of creating the cuckoo filter
                //by using the same function in both instances
                var _ = Convert.FromBase64String(value.Seed);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
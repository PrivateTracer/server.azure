namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerArgs
    {
        public string Token { get; set; }
        public TracerKeyArgs[] Items { get; set; }
    }
}
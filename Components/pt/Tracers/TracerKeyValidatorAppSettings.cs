﻿using System;
using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerKeyValidatorAppSettings : AppSettingsReader, ITracerKeyValidatorConfig
    {
        public TracerKeyValidatorAppSettings(IConfiguration config) : base(config)
        {
        }

        private const string Prefix = "Validation:TracerKey:";
        public int EpochMin => GetValueInt32($"{Prefix}Epoch:Min", 1);
        public int EpochMax => GetValueInt32($"{Prefix}Epoch:Max", int.MaxValue);
        public int KeyByteCount => GetValueInt32(Prefix + nameof(KeyByteCount), 32);
    }
}
﻿namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public interface ITracerKeyValidatorConfig
    {
        int EpochMin { get; }
        int EpochMax { get; }
        int KeyByteCount { get; }
    }
}
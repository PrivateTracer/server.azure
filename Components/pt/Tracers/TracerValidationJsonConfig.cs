using System;
using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerValidationAppSettings : AppSettingsReader, ITracerValidatorConfig
    {
        public TracerValidationAppSettings(IConfiguration config) : base(config)
        {
        }

        public int TracerKeyCountMin => GetValueInt32("Validation:Tracers:ItemCountMin");
        public int TracerKeyCountMax => GetValueInt32("Validation:Tracers:ItemCountMax");
    }
}
namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerKey
    {
        //SKt as per spec
        //32 bytes as base64 == 44 char
        public string Seed { get; set; }
        
        public int Epoch { get; set; }
    }
}
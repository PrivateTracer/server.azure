using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.Components.Tracers
{
    public class TracerAuthorisationQueueSendCommand : QueueSendCommand<TracerAuthorisationArgs>, ITracerAuthorisationWriter
    {
        public TracerAuthorisationQueueSendCommand(IServiceBusConfig sbConfig) : base(sbConfig)
        {
        }
    }
}
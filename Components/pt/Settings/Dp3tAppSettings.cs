using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.ServerLite.Components
{
    public class Dp3tAppSettings : AppSettingsReader, IDp3tConfig
    {
        public Dp3tAppSettings(IConfiguration config) : base(config) { }
        public int TracingSetLifetimeDays => GetValueInt32("DP3T:TracingSetLifetimeDays", 21);
        public int TracerLifetimeDays => GetValueInt32("DP3T:TracerLifetimeDays", 12);
    }
}
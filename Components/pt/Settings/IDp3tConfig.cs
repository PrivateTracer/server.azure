namespace PrivateTracer.Server.Azure.Components.Settings
{
    public interface IDp3tConfig
    {
        int TracingSetLifetimeDays { get; }
        int TracerLifetimeDays { get; }
    }
}
namespace PrivateTracer.Server.Azure.Components.CuckooFilters
{
    public class CfTracerKeyArgs
    {
        public string Seed { get; set; }
        
        public int Epoch { get; set; }
    }
}
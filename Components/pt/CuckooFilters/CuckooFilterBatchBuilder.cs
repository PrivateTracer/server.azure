﻿using System;
using System.Collections.Generic;

namespace PrivateTracer.Server.Azure.Components.CuckooFilters
{
    public sealed class CuckooFilterBatchBuilder : IDisposable
    {
        //TODO private readonly ILogger _Logger;

        //Disposable
        private CuckooFilterBuilder _Builder;
        private bool _Disposed;
        private const string Name = "Unnamed"; //TODO

        /// <summary>
        /// As these are read from the database, we are not repeating validation of individual key properties here.
        /// Does not fail unless result is used.
        /// </summary>
        /// <param name="tracerKeys"></param>
        /// <returns></returns>
        public IEnumerable<string> Build(CfTracerKeyArgs[] tracerKeys)
        {
            if (tracerKeys == null)
                throw new NullReferenceException(nameof(tracerKeys)); //ncrunch: no coverage - C# 8 compiler error

            if (_Disposed)
                throw new ObjectDisposedException(Name);

            _Builder = new CuckooFilterBuilder();

            foreach (var i in tracerKeys)
            {
                    if (_Builder.TryAdd(i))
                        continue;

                    yield return _Builder.Build();
                    _Builder.Dispose();
                    _Builder = new CuckooFilterBuilder();
                    if (!_Builder.TryAdd(i))
                        throw new InvalidOperationException("Entering a key into an empty cuckoo filter should always succeed."); //ncrunch: no coverage Would have to have failed ctor
            }

            if (_Builder.HasContent)
                yield return _Builder.Build();
        }

        public void Dispose()
        {
            if (_Disposed)
                return;

            _Disposed = true;

            _Builder?.Dispose();
        }
    }
}
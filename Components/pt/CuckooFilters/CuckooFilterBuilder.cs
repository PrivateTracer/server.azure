﻿using System;
using System.Runtime.InteropServices;

namespace PrivateTracer.Server.Azure.Components.CuckooFilters
{
    /// <summary>
    /// Rust library wrapper - all externs to the rust library in here.
    /// </summary>
    public sealed class CuckooFilterBuilder : IDisposable
    {
        //TODO these are unmanaged resources and should be SafeHandles?
        //Dispose
        private readonly IntPtr _Test;
        //TODO these are unmanaged resources and should be SafeHandles?
        //Dispose
        private readonly IntPtr _Real;

        private bool _Disposed;
        public string Name = "Barney"; //TODO get a name from the batch job.

        private const string DllName = "dp3t";

        [DllImport(DllName)]
        private static extern IntPtr CF_new();

        [DllImport(DllName)]
        private static extern void CF_free(IntPtr cuckooFilter);

        [DllImport(DllName)]
        private static extern unsafe int CF_add(IntPtr cuckooFilter, byte* value);

        [DllImport(DllName)]
        private static extern int CF_serialize_blob(IntPtr cuckooFilter, [MarshalAs(UnmanagedType.LPStr)]out string outputBlob);
        private const int Success = 0;

        [DllImport(DllName)]
        private static extern void EPH_hash_id(byte[] id, int epochId, byte[] hashedId);

        [DllImport(DllName)]
        private static extern void EPH_generate_id(byte[] seed, byte[] id);

        public CuckooFilterBuilder()
        {
            _Real = CF_new();
            _Test = CF_new();
        }
        /// <summary>
        /// If false is returned, the filter is full and must not call build
        /// </summary>
        /// <param name="cfTracerKeyArgs"></param>
        /// <returns></returns>
        public unsafe bool TryAdd(CfTracerKeyArgs cfTracerKeyArgs)
        {
            if (cfTracerKeyArgs == null)
                throw new ArgumentNullException(nameof(cfTracerKeyArgs)); //ncrunch: no coverage Compiler error

            if (_Disposed)
                throw new ObjectDisposedException(Name);

            if (Full)
                throw new InvalidOperationException("Cuckoo filter is full.");

            fixed (byte* args = CreateAddArgs(cfTracerKeyArgs))
            {
                if (CF_add(_Test, args) != Success)
                {
                    Full = true;
                    return false;
                }

                CF_add(_Real, args);
                HasContent = true;
                return true;
            }
        }

        private static byte[] CreateAddArgs(CfTracerKeyArgs value)
        {
            //NB Use EXACTLY the same function as used to validate when it arrived in the system
            var buffer = Convert.FromBase64String(value.Seed);
            var id = new byte[16];
            var hashedId = new byte[32];

            EPH_generate_id(buffer, id);
            EPH_hash_id(id, value.Epoch, hashedId);

            return hashedId;
        }

        public bool HasContent { get; private set; }
        public bool Full { get; private set; }

        public string Build()
        {
            if (_Disposed)
                throw new ObjectDisposedException(Name);

            if (!HasContent)
                throw new InvalidOperationException();

            if (CF_serialize_blob(_Real, out var result) != Success)
                throw new InvalidOperationException("Failed to build cuckoo filter.");

            return result;
        }

        public void Dispose()
        {
            if (_Disposed) 
                return;

            _Disposed = true;
            if (_Test == IntPtr.Zero) CF_free(_Test);
            if (_Real == IntPtr.Zero) CF_free(_Real);
        }
    }
}
﻿namespace PrivateTracer.Server.Azure.Components.TracingSets
{
    /// <summary>
    /// Must contain the leading /
    /// </summary>
    public static class Routes
    {
        public const string Tracer = "/tracer";
        public const string Config = "/config";
        public const string TracingSets = "/tracingsets";
    }
}
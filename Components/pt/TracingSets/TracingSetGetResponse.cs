namespace PrivateTracer.Server.Azure.Components.TracingSets
{
    public class TracingSetGetResponse
    {
        public string Content { get; set; }
    }
}
using System.Linq;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.TracingSets
{
    public class TracingSetSafeReadCommand
    {
        private readonly IDbConfig _DbConfig;

        public TracingSetSafeReadCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        /// <summary>
        /// Returns null if not found.
        /// </summary>
        /// <param name="tracingSetId"></param>
        /// <returns></returns>
        public TracingSetConfigEntity Execute(string tracingSetId)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return c.Query<TracingSetConfigEntity>()
                .Where(x => x.PublishingId == tracingSetId)
                .Take(1)
                .ToArray()
                .SingleOrDefault();
        }
    }
}

using ProtoBuf;

namespace PrivateTracer.Server.Azure.Components.TracingSets
{
    [ProtoContract]
    public class TracingSetGetResponseV2
    {
        //TODO This is just POC. Real version should try this on the key array
        [ProtoMember(1)]
        public string Content { get; set; }
    }
}
using System;
using System.IO;
using System.Net.Mime;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.Services;
using ProtoBuf;

namespace PrivateTracer.Server.Azure.Components.TracingSets
{
    public class HttpGetTracingSetCommandV2
    {
        private readonly TracingSetSafeReadCommand _SafeReadcommand;

        public HttpGetTracingSetCommandV2(TracingSetSafeReadCommand safeReadcommand)
        {
            _SafeReadcommand = safeReadcommand;
        }

        public IActionResult Execute(string id)
        {
            //if (id < 1)
            //    return new BadRequestResult();

            var tracingSet = _SafeReadcommand.Execute(id);

            if (tracingSet == null)
                return new NotFoundResult();

            var result = new TracingSetGetResponseV2
            {
                Content = tracingSet.CuckooFilter
            };

            return new OkObjectResult(result);
        }
    }
}
﻿using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.TracerAuthorisation
{
    public interface ITracerAuthorisationWriter
    {
        Task Execute(TracerAuthorisationArgs args);
    }
}
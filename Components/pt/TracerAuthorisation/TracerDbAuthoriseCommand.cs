﻿using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.Components.TracerAuthorisation
{
    public class TracerDbAuthoriseCommand : ITracerAuthorisationWriter
    {
        private readonly IDbConfig _DbConfig;

        public TracerDbAuthoriseCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public async Task Execute(TracerAuthorisationArgs args)
        {
            using var c = new CosmosClientEx(_DbConfig);
            var tracer = c.Query<TracerEntity>()
                .Where(x => x.AuthorisationToken == args.Token)
                .Take(1)
                .ToArray()
                .SingleOrDefault();

            if (tracer == null)
                return;

            tracer.Authorised = true;
            await c.UdpateAsync(tracer);
        }
    }
}

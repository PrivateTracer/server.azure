﻿namespace PrivateTracer.Server.Azure.Components.TracerAuthorisation
{
    public class TracerAuthorisationArgs
    {
        public string Token { get; set; }
    }
}
﻿using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{
    public interface ITracingSetMessageSender
    {
        Task Execute(TracingSetMessage message);
    }
}
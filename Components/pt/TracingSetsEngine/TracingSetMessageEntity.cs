﻿using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{
    public class TracingSetMessageEntity : DatabaseEntity
    {
        public TracingSetMessage Message { get; set; }
    }
}
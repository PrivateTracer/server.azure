﻿using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{
    public class TracingSetMessageQueueSender : ITracingSetMessageSender
    {
        private readonly QueueSendCommand<TracingSetMessage> _Sender;

        public TracingSetMessageQueueSender(IServiceBusConfig sbConfig)
        {
            _Sender = new QueueSendCommand<TracingSetMessage>(sbConfig);
        }

        public async Task Execute(TracingSetMessage message)
        {
            await _Sender.Execute(message);
        }
    }
}
﻿using System;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{
    public class TracingSetMessage
    {
        /// <summary>
        /// Sortable data time
        /// </summary>
        public DateTime Created { get; set; }
        public int CreatedQualifier { get; set; }
        public string CuckooFilter { get; set; }
    }
}
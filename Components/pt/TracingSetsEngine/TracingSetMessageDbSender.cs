﻿using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Config;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{

    /// <summary>
    /// Write to a Db instead of a queue
    /// </summary>
    public class TracingSetMessageDbSender : ITracingSetMessageSender
    {
        public TracingSetMessageDbSender(TracingSetDbInsertCommand dbWriter)
        {
            _DbWriter = dbWriter;
        }

        private readonly TracingSetDbInsertCommand _DbWriter;

        public async Task Execute(TracingSetMessage message)
        {
            var e = new TracingSetConfigEntity
            {
                Release = message.Created,
                Qualifier = message.CreatedQualifier,
                CuckooFilter = message.CuckooFilter
            };
            await _DbWriter.Execute(e);
        }
    }
}
﻿using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.Tracers;

//using Microsoft.Azure.Documents;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{
    public class TracerInputEntity : DatabaseEntity
    {
        public string SourceId { get; set; }
        public TracerKeyArgs[] TracerKeys { get; set; }
        
        /// <summary>
        /// Set when the Tracing Set it written
        /// </summary>
        public bool Used { get; set; }
    }
}
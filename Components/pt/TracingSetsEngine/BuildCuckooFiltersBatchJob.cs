﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.CuckooFilters;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Tracers;
using Container = Microsoft.Azure.Cosmos.Container;

namespace PrivateTracer.Server.Azure.Components.TracingSetsEngine
{
    /// <summary>
    /// Add database IO to the job
    /// </summary>
    public sealed class BuildCuckooFiltersBatchJob : IDisposable
    {
        private bool _Disposed;

        private const int UsedCapacity = 300; //248 Tracers of 14 days worth of 5min keys = 1 full cuckoo filter.
        private readonly List<TracerInputEntity> _Used = new List<TracerInputEntity>(UsedCapacity);
        private readonly IDbConfig _SourceDbConfig;
        private readonly IDbConfig _JobDbConfig;
        public string JobName { get; }
        private readonly ITracingSetMessageSender _TracingSetMessageSender;
        private CuckooFilterBuilder _CuckooFilterBuilder;
        private int _Counter;

        private CosmosClientEx _JobDatabase;
        private readonly DateTime _Start;

        public BuildCuckooFiltersBatchJob(IDbConfig sourceDbConfig, IDbConfig jobDbConfig, IUtcDateTimeProvider dateTimeProvider, 
            ITracingSetMessageSender tracingSetMessageSender)
        {
            _SourceDbConfig = sourceDbConfig;
            _JobDbConfig = jobDbConfig;
            _TracingSetMessageSender = tracingSetMessageSender;
            _Start = dateTimeProvider.Now();
            JobName = $"TracingSetsJob_{_Start:u}".Replace(" ", "_");
        }

        public async Task Execute()
        {
            if (_Disposed)
                throw new ObjectDisposedException(JobName);

            await CreateJobDatabase();
            await CopyInputData();
            await BuildCuckooFilters();
            await CommitResults();
        }

        private async Task BuildCuckooFilters()
        {
            _CuckooFilterBuilder = new CuckooFilterBuilder();
            foreach (var i in _JobDatabase.Query<TracerInputEntity>())
            {
                foreach (var j in i.TracerKeys.Select(x=>new CfTracerKeyArgs{Seed = x.Seed, Epoch = x.Epoch}))
                {
                    if (_CuckooFilterBuilder.TryAdd(j))
                        continue;

                    await Build();
                    _CuckooFilterBuilder.Dispose();
                    _CuckooFilterBuilder = new CuckooFilterBuilder();
                    if (!_CuckooFilterBuilder.TryAdd(j))
                        throw new InvalidOperationException("Entering a key into an empty cuckoo filter should always succeed."); //ncrunch: no coverage Would have to have failed ctor
                }

                //Implies that a tracer can be split between 2 cuckoo filters
                //But is not 'used' until all keys are in a cf.
                _Used.Add(i);
            }

            if (_CuckooFilterBuilder.HasContent)
                await Build();
        }

        private async Task CopyInputData()
        {
            using var cosmosClientSource = new CosmosClientEx(_SourceDbConfig);
            var authorisedTracers = cosmosClientSource.Query<TracerEntity>()
                .Where(x => x.Authorised)
                .Select(x => new TracerInputEntity
                {
                    SourceId = x.Id,
                    Region = x.Region,
                    TracerKeys = x.TracerKeys.Select(y => new TracerKeyArgs {Seed = y.Seed, Epoch = y.Epoch}).ToArray()
                });

            foreach (var i in authorisedTracers)
            {
                await _JobDatabase.InsertAsync(i);
            }
        }

        private async Task CreateJobDatabase()
        {
            var client = new CosmosClient(_JobDbConfig.Uri, _JobDbConfig.Token);
            _JobDatabase = new CosmosClientEx(client, JobName);
            await _JobDatabase.CreateDatabaseIfNotExistsAsync();
            await _JobDatabase.CreateContainerIfNotExistsAsync<TracerInputEntity>();
            await _JobDatabase.CreateContainerIfNotExistsAsync<TracingSetMessageEntity>();
        }

        private async Task CommitResults()
        {
            //Write TracingSet to Config Engine Queue.
            foreach (var i in _JobDatabase.Query<TracingSetMessageEntity>())
            {
                await _TracingSetMessageSender.Execute(i.Message);
            }

            //Delete tracers that were included in a cuckoo filter.
            var q = _JobDatabase.Query<TracerInputEntity>()
                .Where(x => x.Used)
                .Select(x => x.SourceId)
                .ToArray(); //TODO prefer not

            using (var cosmosClientSource = new CosmosClientEx( _SourceDbConfig))
            {
                foreach (var i in q)
                    await cosmosClientSource.DeleteAsync<TracerEntity>(i);
            }

            await _JobDatabase.DeleteDatabaseAsnyc();
        }

        private async Task Build()
        {
            var content = _CuckooFilterBuilder.Build();
            var m = new TracingSetMessage
            {
                Created = _Start,
                CreatedQualifier = ++_Counter,
                CuckooFilter = content
            };
            var e = new TracingSetMessageEntity 
            {
                Message = m
            };
            await _JobDatabase.InsertAsync(e);
            await WriteUsed();
        }

        private async Task WriteUsed()
        {
            foreach (var i in _Used)
            {
                i.Used = true;
                await _JobDatabase.UdpateAsync(i);
            }

            _Used.Clear();
        }

        public void Dispose()
        {
            if (_Disposed)
                return;

            _Disposed = true;
            _JobDatabase?.Dispose();
            _CuckooFilterBuilder?.Dispose();
        }
    }
}
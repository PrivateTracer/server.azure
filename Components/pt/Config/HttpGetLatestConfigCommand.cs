﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class HttpGetLatestConfigCommand
    {
        private readonly ConfigBuilder _ConfigBuilder;

        public HttpGetLatestConfigCommand(ConfigBuilder configBuilder)
        {
            _ConfigBuilder = configBuilder;
        }

        public IActionResult Execute()
        {
            //TODO check content type is set - change to JsonResult?
            return new OkObjectResult(JsonConvert.SerializeObject(_ConfigBuilder.Execute()));
        }
    }
}
using System;
using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracersExpireCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _UtcDateTimeProvider;
        private readonly IDp3tConfig _Dp3TConfig;

        public TracersExpireCommand(IDbConfig config, IUtcDateTimeProvider utcDateTimeProvider, IDp3tConfig dp3TConfig)
        {
            _DbConfig = config;
            _UtcDateTimeProvider = utcDateTimeProvider;
            _Dp3TConfig = dp3TConfig;
        }

        public async Task Execute()
        {
            var now = _UtcDateTimeProvider.Now();

            using var c = new CosmosClientEx(_DbConfig);

            var q = c.Query<TracerEntity>()
                .Where(x => x.Created < now - TimeSpan.FromDays(_Dp3TConfig.TracerLifetimeDays))
                .Select(x => new { x.Id, x.Region})
                .ToArray();

            foreach (var i in q)
            {
                await c.DeleteAsync<TracerEntity>(i.Id);
            }
        }
    }
}
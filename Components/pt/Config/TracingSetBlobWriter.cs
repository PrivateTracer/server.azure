using System;
using System.IO;
using System.Text;
using Microsoft.Azure.Storage.Auth;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracingSetBlobWriter
    {
        private readonly IStorageAccountConfig _StorageAccountConfig;

        public TracingSetBlobWriter(IStorageAccountConfig storageAccountConfig)
        {
            _StorageAccountConfig = storageAccountConfig;
        }

        public void Execute(TracingSetConfigEntity e)
        {
            var blobClient = new StorageClientEx(_StorageAccountConfig);
            using var input = new MemoryStream(Encoding.UTF8.GetBytes(e.CuckooFilter));
            blobClient.Write(input, "config", "config");
        }
    }
}
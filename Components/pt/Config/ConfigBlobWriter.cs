using System;
using System.IO;
using System.Text;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class ConfigBlobWriter
    {
        private readonly IStorageAccountConfig _StorageAccountConfig;

        public ConfigBlobWriter(IStorageAccountConfig storageAccountConfig)
        {
            _StorageAccountConfig = storageAccountConfig;
        }

        public void Execute(ConfigResponse e)
        {
            var storageAccountConfig = CloudStorageAccount.Parse(_StorageAccountConfig.ConnectionString); //HACK
            var blobClient = new CloudBlobClient(storageAccountConfig.BlobEndpoint, storageAccountConfig.Credentials);
            var container = blobClient.GetContainerReference("config"); //TODO Root not container...
            var blob = container.GetBlockBlobReference("config");
            using var input = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e)));
            try
            {
                blob.UploadFromStream(input);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}
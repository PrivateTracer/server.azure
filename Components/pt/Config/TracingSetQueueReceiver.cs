using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracingSetQueueReceiver
    {
        public TracingSetQueueReceiver(TracingSetDbInsertCommand dbWriter, TracingSetBlobWriter blobWriter)
        {
            _DbWriter = dbWriter;
            _BlobWriter = blobWriter;
        }

        private readonly TracingSetBlobWriter _BlobWriter;
        private readonly TracingSetDbInsertCommand _DbWriter;

        public async Task Execute(TracingSetMessage message)
        {
            var e = new TracingSetConfigEntity
            {
                Release = message.Created,
                Qualifier = message.CreatedQualifier,
                CuckooFilter = message.CuckooFilter
            };
            e = await _DbWriter.Execute(e);
            _BlobWriter.Execute(e);
        }
    }
}
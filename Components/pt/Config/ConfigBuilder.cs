using PrivateTracer.Server.Azure.Components.RivmAdvice;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class ConfigBuilder
    {
        private readonly TracingSetsConfigBuilder _TracingSetsConfigBuilder;
        private readonly GetLatestRivmAdviceCommand _RivmAdviceFinder;
        private readonly GetLatestRssiThresholdsCommand _RssiThresholdsFinder;

        public ConfigBuilder(TracingSetsConfigBuilder tracingSetsConfigBuilder, GetLatestRivmAdviceCommand rivmAdviceFinder, GetLatestRssiThresholdsCommand rssiThresholdsFinder)
        {
            _TracingSetsConfigBuilder = tracingSetsConfigBuilder;
            _RivmAdviceFinder = rivmAdviceFinder;
            _RssiThresholdsFinder = rssiThresholdsFinder;
        }

        public ConfigResponse Execute()
        {
            return new ConfigResponse 
            { 
                TracingSets = _TracingSetsConfigBuilder.Execute(),
                RivmAdvice = _RivmAdviceFinder.Execute(),
                RssiThresholds = _RssiThresholdsFinder.Execute()
            };
        }
    }
}
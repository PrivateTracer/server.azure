using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracingSetDbInsertCommand
    {
        private readonly IDbConfig _DbConfig;

        public TracingSetDbInsertCommand(IDbConfig config)
        {
            _DbConfig = config;
        }

        public async Task<TracingSetConfigEntity> Execute(TracingSetConfigEntity args)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return await c.InsertAsync(args);
        }
    }
}
namespace PrivateTracer.Server.Azure.Components.Config
{
    public class ConfigUpdateWriter
    {
        private readonly ConfigBuilder _ConfigBuilder;
        private readonly ConfigBlobWriter _Writer;

        public ConfigUpdateWriter(ConfigBuilder configBuilder, ConfigBlobWriter writer)
        {
            _ConfigBuilder = configBuilder;
            _Writer = writer;
        }

        public void Execute()
        {
            _Writer.Execute(_ConfigBuilder.Execute());
        }
    }
}
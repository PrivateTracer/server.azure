using System;
using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracingSetExpireCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _UtcDateTimeProvider;
        private readonly IDp3tConfig _Dp3TConfig;
        private readonly IStorageAccountConfig _StorageAccountConfig;

        public TracingSetExpireCommand(IDbConfig config, IUtcDateTimeProvider utcDateTimeProvider, IDp3tConfig dp3TConfig, IStorageAccountConfig storageAccountConfig)
        {
            _DbConfig = config;
            _UtcDateTimeProvider = utcDateTimeProvider;
            _Dp3TConfig = dp3TConfig;
            _StorageAccountConfig = storageAccountConfig;
        }

        public async Task Execute()
        {
            var expired = _UtcDateTimeProvider.Now() - TimeSpan.FromDays(_Dp3TConfig.TracingSetLifetimeDays);
            var blobClient = new StorageClientEx(_StorageAccountConfig);
            using var c = new CosmosClientEx(_DbConfig);

            var q = c.Query<TracingSetConfigEntity>()
                .Where(x => x.Release < expired)
                .Select(x => new {x.Id, x.PublishingId})
                .ToArray();

            foreach (var i in q)
            {
                await c.DeleteAsync<TracingSetConfigEntity>(i.Id);
                await blobClient.DeleteAsync("tracingsets", i.PublishingId);
            }
        }
    }
}
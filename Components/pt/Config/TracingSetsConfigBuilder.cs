using System;
using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracingSetsConfigBuilder
    {
        private readonly IDbConfig _DbConfig;
        private readonly IDp3tConfig _Dp3TConfig;

        public TracingSetsConfigBuilder(IDbConfig dbConfig, IDp3tConfig dp3TConfig)
        {
            _DbConfig = dbConfig;
            _Dp3TConfig = dp3TConfig;
        }

        public TracingSetsConfig Execute()
        {
            var expired = new StandardUtcDateTimeProvider().Now() - TimeSpan.FromDays(_Dp3TConfig.TracingSetLifetimeDays);
            using (var client = new CosmosClientEx(_DbConfig))
            {
                var activeTracingSets = client.Query<TracingSetConfigEntity>()
                    .Where(x => x.Release > expired)
                    .Select(x => x.PublishingId)
                    .ToArray();

                return new TracingSetsConfig
                {
                    //TODO Uri = new Uri(new Uri(_StorageAccountConfig.Uri, UriKind.Absolute), _StorageAccountConfig.TracingSetsContainerName).AbsoluteUri,
                    Ids = activeTracingSets
                };
            }
        }
    }
}
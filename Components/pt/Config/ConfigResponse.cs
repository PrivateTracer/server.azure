﻿namespace PrivateTracer.Server.Azure.Components.Config
{
    public class ConfigResponse
    {
        public TracingSetsConfig TracingSets { get; set; }
        public string RivmAdvice { get; set; }
        public string RssiThresholds { get; set; }
    }
}
using System;
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class TracingSetConfigEntity : PublishedDatabaseEntity
    {
        /// <summary>
        /// Metadata
        /// </summary>
        public int Qualifier { get; set; }
        
        /// <summary>
        /// Content
        /// </summary>
        public string CuckooFilter { get; set; }
    }
}
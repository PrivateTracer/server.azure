﻿using System.Linq;
using PrivateTracer.Server.Azure.Components.Calibration;
using PrivateTracer.Server.Azure.Components.RivmAdvice;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public class GetLatestRssiThresholdsCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _DateTimeProvider;

        public GetLatestRssiThresholdsCommand(IDbConfig dbConfig, IUtcDateTimeProvider dateTimeProvider)
        {
            _DbConfig = dbConfig;
            _DateTimeProvider = dateTimeProvider;
        }

        public string Execute()
        {
            using var c = new CosmosClientEx(_DbConfig);
            var now = _DateTimeProvider.Now();
            return c.Query<RssiThresholdConfigEntity>()
                .Where(x => x.Release <= now)
                .OrderByDescending(x => x.Release)
                .Take(1)
                .Select(x => x.PublishingId)
                .ToArray()
                .SingleOrDefault();
        }
    }
}
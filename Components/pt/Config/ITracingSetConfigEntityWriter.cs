using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.Config
{
    public interface ITracingSetConfigEntityWriter
    {
        Task<TracingSetConfigEntity> Execute(TracingSetConfigEntity args);
    }
}
namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    /// <summary>
    /// Diagnosis key
    /// </summary>
    public class AgTracerArgs
    {
        public string Token { get; set; }
        public AgTracerKeyArgs[] Items { get; set; }
    }
}
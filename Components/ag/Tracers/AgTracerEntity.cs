using System;
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerEntity : DatabaseEntity
    {
        public AgTracerKey[] TracerKeys { get; set; }
        public string AuthorisationToken { get; set; }

        /// <summary>
        /// False by default, authorized via CPS
        /// </summary>
        public bool Authorised { get; set; }

        public DateTime Created { get; set; }
    }
}
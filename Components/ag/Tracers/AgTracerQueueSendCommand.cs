using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerQueueSendCommand : QueueSendCommand<AgTracerArgs>, IAgTracerWriter
    {
        public AgTracerQueueSendCommand(IServiceBusConfig sbConfig) : base(sbConfig)
        {
        }
    }
}
namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    
    /// <summary>
    /// DbEntity
    /// </summary>
    public class AgTracerKey
    {
        /// <summary>
        /// TemporaryExposureKey
        /// The 'ephemeral key' is called the RollingProximityIdentifier
        /// </summary>
        public string DailyKey { get; set; }

        /// <summary>
        /// Equivalent of Dp3T Epoch
        /// </summary>
        public int RollingStart { get; set; }

        /// <summary>
        /// Number of epochs in 24hours
        /// Currently fixed at 144? e.g. 10mins
        /// </summary>
        public int RollingPeriod { get; set; }

        /// <summary>
        /// Yet to be well defined.
        /// Self-diagnosis support?
        /// Comes from server?
        /// Phone can transmit risk level.
        /// 1-8 or 0-100
        /// </summary>
        public int Risk { get; set; }
    }
}
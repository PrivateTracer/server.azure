﻿using System;
using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerKeyValidatorAppSettingsConfig : AppSettingsReader, IAgTracerKeyValidatorConfig
    {
        public AgTracerKeyValidatorAppSettingsConfig(IConfiguration config) : base(config)
        {
        }

        private const string Prefix = "Validation:AgTracerKey:";

        public int RollingPeriodMin => GetValueInt32(Prefix+nameof(RollingPeriodMin));
        public int RollingPeriodMax => GetValueInt32(Prefix + nameof(RollingPeriodMax));
        public int DailyKeyByteCount => GetValueInt32(Prefix + nameof(DailyKeyByteCount));
    }
}
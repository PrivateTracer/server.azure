namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public interface IAgTracerValidatorConfig
    {
        int TracerKeyCountMin { get; }
        int TracerKeyCountMax { get; }
    }
}
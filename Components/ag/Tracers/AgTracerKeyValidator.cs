﻿using System;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerKeyValidator : IAgTracerKeyValidator
    {
        private readonly IAgTracerKeyValidatorConfig _Config;

        public AgTracerKeyValidator(IAgTracerKeyValidatorConfig config)
        {
            _Config = config;
        }

        public bool Valid(AgTracerKeyArgs value)
        {
            if (value == null)
                return false;

            if (_Config.RollingPeriodMin > value.RollingPeriod || value.RollingPeriod > _Config.RollingPeriodMax)
                return false;

            //TODO valid values epoch size, currently 10mins, for value.RollingStart 

            if (string.IsNullOrEmpty(value.DailyKey))
                return false;

            try
            {
                //GUARANTEES a successful conversion at the point of creating the cuckoo filter
                //by using the same function in both instances
                var bytes = Convert.FromBase64String(value.DailyKey);

                if (bytes.Length != _Config.DailyKeyByteCount)
                    return false;
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }
    }
}
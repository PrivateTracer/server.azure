using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.ag.Mapping;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerDbInsertCommand : IAgTracerWriter
    {
        private readonly IDbConfig _DbConfig;

        public AgTracerDbInsertCommand(IDbConfig config)
        {
            _DbConfig = config;
        }

        public async Task Execute(AgTracerArgs args)
        {
            using var client = new CosmosClientEx(_DbConfig);
            var e = args.ToDbEntity();
            await client.InsertAsync(e);
        }
    }
}

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public interface IAgTracerValidator
    {
        bool Validate(AgTracerArgs args);
    }
}
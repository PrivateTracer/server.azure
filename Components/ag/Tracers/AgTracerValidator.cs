using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerValidator : IAgTracerValidator
    {
        private readonly IAgTracerValidatorConfig _Config;
        private readonly ITracerAuthorisationTokenValidator _AuthorisationTokenValidator;
        private readonly IAgTracerKeyValidator _TracerKeyValidator;

        public AgTracerValidator(IAgTracerValidatorConfig config, ITracerAuthorisationTokenValidator authorisationTokenValidator, IAgTracerKeyValidator tracerKeyValidator)
        {
            _Config = config;
            _AuthorisationTokenValidator = authorisationTokenValidator;
            _TracerKeyValidator = tracerKeyValidator;
        }

        public bool Validate(AgTracerArgs args)
        {
            if (args == null)
                return false;

            if (!_AuthorisationTokenValidator.IsValid(args.Token))
                return false;

            if (_Config.TracerKeyCountMin > args.Items.Length
                || args.Items.Length > _Config.TracerKeyCountMax)
                return false;

            return args.Items.All(_TracerKeyValidator.Valid);
        }
    }
}
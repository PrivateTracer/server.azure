using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerAuthorisationQueueSendCommand : QueueSendCommand<TracerAuthorisationArgs>, ITracerAuthorisationWriter
    {
        public AgTracerAuthorisationQueueSendCommand(IServiceBusConfig sbConfig) : base(sbConfig)
        {
        }
    }
}
﻿namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public interface IAgTracerKeyValidatorConfig
    {
        int RollingPeriodMin { get; }
        int RollingPeriodMax { get; }
        int DailyKeyByteCount { get; }
    }
}
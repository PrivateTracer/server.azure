using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public interface IAgTracerWriter
    {
        Task Execute(AgTracerArgs args);
    }
}
using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class HttpPostAgTracerCommand
    {
        private readonly IAgTracerWriter _TracerDbInsertCommand;
        private readonly IAgTracerValidator _TracerValidator;

        public HttpPostAgTracerCommand(IAgTracerWriter infectionDbInsertCommand, IAgTracerValidator tracerValidator)
        {
            _TracerDbInsertCommand = infectionDbInsertCommand;
            _TracerValidator = tracerValidator;
        }

        public IActionResult Execute(AgTracerArgs args)
        {
            if (!_TracerValidator.Validate(args))
                return new BadRequestResult();

            _TracerDbInsertCommand.Execute(args).GetAwaiter().GetResult();
            return new OkResult();
        }
    }
}
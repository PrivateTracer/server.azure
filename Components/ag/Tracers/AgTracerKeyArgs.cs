namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerKeyArgs
    {
        public string DailyKey { get; set; }
        public int RollingStart { get; set; }
        public int RollingPeriod { get; set; }
        public int Risk { get; set; }
    }
}
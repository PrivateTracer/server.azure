using System;
using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public class AgTracerValidationAppSettings : AppSettingsReader, IAgTracerValidatorConfig
    {
        public AgTracerValidationAppSettings(IConfiguration config) : base(config)
        {
        }
        public int TracerKeyCountMin => GetValueInt32("Validation:AgTracers:ItemCountMin", 1);
        public int TracerKeyCountMax => GetValueInt32("Validation:AgTracers:ItemCountMax", 21);
    }
}
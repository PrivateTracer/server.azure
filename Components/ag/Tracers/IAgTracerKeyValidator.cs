﻿namespace PrivateTracer.Server.Azure.Components.ag.Tracers
{
    public interface IAgTracerKeyValidator
    {
        bool Valid(AgTracerKeyArgs value);
    }
}
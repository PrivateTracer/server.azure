using System.Linq;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSets
{
    public class AgTracingSetSafeReadCommand
    {
        private readonly IDbConfig _DbConfig;

        public AgTracingSetSafeReadCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        /// <summary>
        /// Returns null if not found.
        /// </summary>
        /// <param name="tracingSetId"></param>
        /// <returns></returns>
        public AgTracingSetConfigEntity Execute(string tracingSetId)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return c.Query<AgTracingSetConfigEntity>()
                .Where(x => x.PublishingId == tracingSetId)
                .Take(1)
                .ToArray()
                .SingleOrDefault();
        }
    }
}

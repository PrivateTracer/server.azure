﻿namespace PrivateTracer.Server.Azure.Components.ag.TracingSets
{
    /// <summary>
    /// Must contain the leading /
    /// </summary>
    public static class AgRoutes
    {
        public const string Tracer = "ag/tracer";
        public const string Config = "ag/config";
        public const string TracingSets = "ag/tracingsets";
    }
}
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSets
{
    public class HttpGetAgTracingSetCommand
    {
        private readonly AgTracingSetSafeReadCommand _SafeReadcommand;

        public HttpGetAgTracingSetCommand(AgTracingSetSafeReadCommand safeReadcommand)
        {
            _SafeReadcommand = safeReadcommand;
        }

        public IActionResult Execute(string id)
        {
            var tracingSet = _SafeReadcommand.Execute(id);

            if (tracingSet == null)
                return new NotFoundResult();

            var result = new AgTracingSetGetResponse 
            { 
                Items = tracingSet.Items
                    .Select(x => new AgTracerKeyReponse { 
                        DailyKey = Convert.FromBase64String(x.DailyKey), 
                        RollingPeriod = x.RollingPeriod, 
                        Risk = x.Risk, 
                        RollingStart = x.RollingStart
                    } ).ToArray()
            };

            return new OkObjectResult(result);
        }
    }
}
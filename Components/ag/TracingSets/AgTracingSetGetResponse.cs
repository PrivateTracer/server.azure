using PrivateTracer.Server.Azure.Components.ag.Tracers;
using ProtoBuf;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSets
{
    [ProtoContract]
    public class AgTracingSetGetResponse
    {
        [ProtoMember(1)]
        public AgTracerKeyReponse[] Items { get; set; }
    }

    [ProtoContract]
    public class AgTracerKeyReponse
    {
        [ProtoMember(1)]
        public byte[] DailyKey { get; set; }

        /// <summary>
        /// TODO unsigned short?
        /// </summary>
        [ProtoMember(2)]
        public int RollingStart { get; set; }

        /// <summary>
        /// TODO unsigned short?
        /// </summary>
        [ProtoMember(3)]
        public int RollingPeriod { get; set; }

        /// <summary>
        /// TODO byte?
        /// </summary>
        [ProtoMember(4)]
        public int Risk { get; set; }
    }
}
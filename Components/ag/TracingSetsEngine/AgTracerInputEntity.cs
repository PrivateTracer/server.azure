﻿using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.DbEntities;

//using Microsoft.Azure.Documents;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{

    public class AgTracerInputEntity : DatabaseEntity
    {
        public string SourceId { get; set; }

        public AgTracerKey[] TracerKeys { get; set; }
        
        /// <summary>
        /// Set when the Tracing Set it written
        /// </summary>
        public bool Used { get; set; }
    }
}
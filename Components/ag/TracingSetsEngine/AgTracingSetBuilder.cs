﻿using System.Collections.Generic;
using PrivateTracer.Server.Azure.Components.ag.Tracers;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{
    public class AgTracingSetBuilder
    {
        private readonly List<AgTracerKey> _Items;

        public AgTracingSetBuilder(int itemCount)
        {
            _Items = new List<AgTracerKey>(itemCount);
        }

        public bool TryAdd(AgTracerKey item)
        {
            if (_Items.Count == _Items.Capacity)
                return false;
            
            _Items.Add(item);
            return true;
        }

        public bool HasContent => _Items.Count > 0;

        public AgTracerKey[] Build()
        {
            return _Items.ToArray();
        }
    }
}
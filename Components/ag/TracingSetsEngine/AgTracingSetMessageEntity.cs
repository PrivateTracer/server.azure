﻿using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{
    public class AgTracingSetMessageEntity : DatabaseEntity
    {
        public AgTracingSetMessage Message { get; set; }
    }
}
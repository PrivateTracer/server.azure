﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{



    /// <summary>
    /// Add database IO to the job
    /// </summary>
    public sealed class BuildAgTracingSetBatchJob : IDisposable
    {
        private bool _Disposed;

        private const int UsedCapacity = 300; //248 Tracers of 14 days worth of 5min keys = 1 full cuckoo filter.
        private readonly List<AgTracerInputEntity> _Used = new List<AgTracerInputEntity>(UsedCapacity);
        private readonly IDbConfig _SourceDbConfig;
        private readonly IDbConfig _JobDbConfig;
        public string JobName { get; }
        private readonly IAgTracingSetMessageSender _TracingSetMessageSender;

        private readonly IAgConfig _AgConfig;

        private AgTracingSetBuilder _SetBuilder;
        private int _Counter;

        private CosmosClientEx _JobDatabase;
        private readonly DateTime _Start;

        public BuildAgTracingSetBatchJob(IDbConfig sourceDbConfig, IDbConfig jobDbConfig, IUtcDateTimeProvider dateTimeProvider, 
            IAgTracingSetMessageSender tracingSetMessageSender, IAgConfig agConfig)
        {
            _SourceDbConfig = sourceDbConfig;
            _JobDbConfig = jobDbConfig;
            _TracingSetMessageSender = tracingSetMessageSender;
            _AgConfig = agConfig;
            _Start = dateTimeProvider.Now();
            JobName = $"TracingSetsJob_{_Start:u}".Replace(" ", "_");
        }

        public async Task Execute()
        {
            if (_Disposed)
                throw new ObjectDisposedException(JobName);

            await CreateJobDatabase();
            await CopyInputData();
            await BuildBatches();
            await CommitResults();
        }




        private async Task BuildBatches()
        {
            _SetBuilder = new AgTracingSetBuilder(_AgConfig.TracingSetCapacity);
            foreach (var i in _JobDatabase.Query<AgTracerInputEntity>())
            {
                foreach (var j in i.TracerKeys
                    //.Select(x => new AgTracerKeyArgs { DailyKey = x.DailyKey, RollingPeriod = x.RollingPeriod, RollingStart = x.RollingStart, Risk = x.Risk})
                )
                {
                    if (_SetBuilder.TryAdd(j))
                        continue;

                    await Build();
                    _SetBuilder = new AgTracingSetBuilder(_AgConfig.TracingSetCapacity);
                }
                _Used.Add(i);
            }

            if (_SetBuilder.HasContent)
                await Build();
        }

        private async Task CopyInputData()
        {
            using var cosmosClientSource = new CosmosClientEx(_SourceDbConfig);
            var authorisedTracers = cosmosClientSource.Query<AgTracerEntity>()
                .Where(x => x.Authorised)
                .Select(x => new AgTracerInputEntity
                {
                    SourceId = x.Id,
                    Region = x.Region,
                    TracerKeys = x.TracerKeys
                });

            foreach (var i in authorisedTracers)
            {
                await _JobDatabase.InsertAsync(i);
            }
        }

        private async Task CreateJobDatabase()
        {
            var client = new CosmosClient(_JobDbConfig.Uri, _JobDbConfig.Token);
            _JobDatabase = new CosmosClientEx(client, JobName);
            await _JobDatabase.CreateDatabaseIfNotExistsAsync();
            await _JobDatabase.CreateContainerIfNotExistsAsync<AgTracerInputEntity>();
            await _JobDatabase.CreateContainerIfNotExistsAsync<AgTracingSetMessageEntity>();
        }

        private async Task CommitResults()
        {
            //Write TracingSet to Config Engine Queue.
            foreach (var i in _JobDatabase.Query<AgTracingSetMessageEntity>())
            {
                await _TracingSetMessageSender.Execute(i.Message);
            }

            //Delete tracers that were included in a tracing set.
            var q = _JobDatabase.Query<AgTracerInputEntity>()
                .Where(x => x.Used)
                .Select(x => x.SourceId)
                .ToArray(); //TODO prefer not

            using (var cosmosClientSource = new CosmosClientEx( _SourceDbConfig))
            {
                foreach (var i in q)
                    await cosmosClientSource.DeleteAsync<AgTracerEntity>(i);
            }

            await _JobDatabase.DeleteDatabaseAsnyc();
        }

        private async Task Build()
        {
            ///TODO
            var m = new AgTracingSetMessage
            {
                Created = _Start,
                CreatedQualifier = ++_Counter,
                Items = _SetBuilder.Build()
            };

            var e = new AgTracingSetMessageEntity 
            {
                Message = m
            };
            await _JobDatabase.InsertAsync(e);
            await WriteUsed();
        }

        private async Task WriteUsed()
        {
            foreach (var i in _Used)
            {
                i.Used = true;
                await _JobDatabase.UdpateAsync(i);
            }

            _Used.Clear();
        }

        public void Dispose()
        {
            if (_Disposed)
                return;

            _Disposed = true;
            _JobDatabase?.Dispose();
        }
    }
}
﻿using System;
using PrivateTracer.Server.Azure.Components.ag.Tracers;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{
    public class AgTracingSetMessage
    {
        /// <summary>
        /// Sortable data time
        /// </summary>
        public DateTime Created { get; set; }
        public int CreatedQualifier { get; set; }
        public AgTracerKey[] Items { get; set; }
    }
}
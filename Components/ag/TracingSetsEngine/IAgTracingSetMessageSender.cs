﻿using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{
    public interface IAgTracingSetMessageSender
    {
        Task Execute(AgTracingSetMessage message);
    }
}
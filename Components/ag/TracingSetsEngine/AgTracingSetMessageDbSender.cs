﻿using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.ag.Mapping;

namespace PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine
{

    /// <summary>
    /// Write to a Db instead of a queue
    /// </summary>
    public class AgTracingSetMessageDbSender : IAgTracingSetMessageSender
    {
        public AgTracingSetMessageDbSender(AgTracingSetDbInsertCommand dbWriter)
        {
            _DbWriter = dbWriter;
        }

        private readonly AgTracingSetDbInsertCommand _DbWriter;

        public async Task Execute(AgTracingSetMessage message)
        {
            var e = message.ToDbEntity();
            await _DbWriter.Execute(e);
        }
    }
}
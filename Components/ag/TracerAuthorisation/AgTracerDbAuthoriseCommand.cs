﻿using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.Components.ag.TracerAuthorisation
{
    public class AgTracerDbAuthoriseCommand : IAgTracerAuthorisationWriter
    {
        private readonly IDbConfig _DbConfig;

        public AgTracerDbAuthoriseCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public async Task Execute(TracerAuthorisationArgs args)
        {
            using var c = new CosmosClientEx(_DbConfig);
            var tracer = c.Query<AgTracerEntity>() //ONLY THIS LINE IS DIFFERENT
                .Where(x => x.AuthorisationToken == args.Token)
                .Take(1)
                .ToArray()
                .SingleOrDefault();

            if (tracer == null)
                return;

            tracer.Authorised = true;
            await c.UdpateAsync(tracer);
        }
    }
}

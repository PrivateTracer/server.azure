﻿using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.Components.ag.TracerAuthorisation
{
    /// <summary>
    /// TODO kill this and use ioc factory registration
    /// </summary>
    public interface IAgTracerAuthorisationWriter
    {
        Task Execute(TracerAuthorisationArgs args);
    }
}
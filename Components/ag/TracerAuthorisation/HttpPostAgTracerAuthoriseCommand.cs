﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.Components.ag.TracerAuthorisation
{
    public class HttpPostAgTracerAuthoriseCommand
    {
        private readonly ITracerAuthorisationTokenValidator _TracerAuthorisationTokenValidator;
        private readonly IAgTracerAuthorisationWriter _TracerAuthorisationWriter;
        public HttpPostAgTracerAuthoriseCommand(IAgTracerAuthorisationWriter tracerAuthorisationWriter, ITracerAuthorisationTokenValidator tracerAuthorisationTokenValidator)
        {
            _TracerAuthorisationWriter = tracerAuthorisationWriter;
            _TracerAuthorisationTokenValidator = tracerAuthorisationTokenValidator;
        }

        public IActionResult Execute(TracerAuthorisationArgs args)
        {
            if (!ValidateAndCleanRequestMessage(args))
                return new BadRequestResult();

            _TracerAuthorisationWriter.Execute(args);
            return new OkResult();
        }

        private bool ValidateAndCleanRequestMessage(TracerAuthorisationArgs args)
        {
            if (args == null)
                return false;

            //Allow hyphens
            var token = args.Token?.Trim().Replace("-", string.Empty);

            if (!_TracerAuthorisationTokenValidator.IsValid(token))
                return false;

            args.Token = token;
            return true;
        }
    }
}

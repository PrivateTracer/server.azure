﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine;

namespace PrivateTracer.Server.Azure.Components.ag.Mapping
{
    public static class MappingStuff
    {
        public static AgTracingSetConfigEntity ToDbEntity(this AgTracingSetMessage message)
        {
            return new AgTracingSetConfigEntity
            {
                Release = message.Created,
                Qualifier = message.CreatedQualifier,
                Items = message.Items
            };
        }
        public static AgTracerEntity ToDbEntity(this AgTracerArgs args)
        {
            return new AgTracerEntity
            {
                TracerKeys = args.Items.Select(ToDbEntity).ToArray(),
                AuthorisationToken = args.Token,
            };
        }

        private static AgTracerKey ToDbEntity(AgTracerKeyArgs args)
            => new AgTracerKey
            {
                DailyKey = args.DailyKey,
                RollingPeriod = args.RollingPeriod,
                RollingStart = args.RollingStart,
                Risk = args.Risk
            };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrivateTracer.Server.Azure.Components.ag
{
    public interface IAgConfig
    {
        int TracerLifetimeDays { get; }
        int TracingSetLifetimeDays { get; }
        int TracingSetCapacity { get; }
    }
}

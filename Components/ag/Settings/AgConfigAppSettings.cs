using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.Components.ag;

namespace PrivateTracer.Server.Azure.ServerLite.Components
{
    public class AgConfigAppSettings : AppSettingsReader, IAgConfig
    {
        public AgConfigAppSettings(IConfiguration config) : base(config) { }

        public int TracingSetCapacity => GetValueInt32("Ag:TracingSet:Capacity", 21);
        public int TracingSetLifetimeDays => GetValueInt32("Ag:TracingSet:LifetimeDays", 21);
        public int TracerLifetimeDays => GetValueInt32("Ag:Tracer:LifetimeDays", 12);
    }
}
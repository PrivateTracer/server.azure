using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class AgTracingSetExpireCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _UtcDateTimeProvider;
        private readonly IAgConfig _AgConfig;
        private readonly IStorageAccountConfig _StorageAccountConfig;

        public AgTracingSetExpireCommand(IDbConfig config, IUtcDateTimeProvider utcDateTimeProvider, IAgConfig agConfig, IStorageAccountConfig storageAccountConfig)
        {
            _DbConfig = config;
            _UtcDateTimeProvider = utcDateTimeProvider;
            _AgConfig = agConfig;
            _StorageAccountConfig = storageAccountConfig;
        }

        public async Task Execute()
        {
            var now = _UtcDateTimeProvider.Now();

            var blobClient = new StorageClientEx(_StorageAccountConfig);

            using var c = new CosmosClientEx(_DbConfig);
            var q = c.Query<AgTracingSetConfigEntity>()
                .Where(x => x.Release < now - TimeSpan.FromDays(_AgConfig.TracingSetLifetimeDays))
                .Select(x => new {x.Id, x.PublishingId})
                .ToArray();

            foreach (var i in q)
            {
                await c.DeleteAsync<AgTracingSetConfigEntity>(i.Id);
                await blobClient.DeleteAsync("tracingsets", i.PublishingId);
            }
        }
    }
}
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.RivmAdvice;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class AgConfigBuilder
    {
        private readonly AgTracingSetsConfigBuilder _TracingSetsConfigBuilder;
        private readonly GetLatestRivmAdviceCommand _RivmAdviceFinder;
        private readonly GetLatestRssiThresholdsCommand _RssiThresholdsFinder;
        private readonly GetLatestTracingCalcConfigCommand _TracerCalcConfigFinder;

        public AgConfigBuilder(AgTracingSetsConfigBuilder tracingSetsConfigBuilder, GetLatestRivmAdviceCommand rivmAdviceFinder, GetLatestRssiThresholdsCommand rssiThresholdsFinder, GetLatestTracingCalcConfigCommand tracerCalcConfigFinder)
        {
            _TracingSetsConfigBuilder = tracingSetsConfigBuilder;
            _RivmAdviceFinder = rivmAdviceFinder;
            _RssiThresholdsFinder = rssiThresholdsFinder;
            _TracerCalcConfigFinder = tracerCalcConfigFinder;
        }

        public AgConfigResponse Execute()
        {
            return new AgConfigResponse 
            { 
                TracingSets = _TracingSetsConfigBuilder.Execute(),
                RivmAdvice = _RivmAdviceFinder.Execute(),
                RssiThresholds = _RssiThresholdsFinder.Execute(),
                TracingCalcConfig = _TracerCalcConfigFinder.Execute(),
            };
        }
    }
}
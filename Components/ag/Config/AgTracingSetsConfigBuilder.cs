using System;
using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class AgTracingSetsConfigBuilder
    {
        private readonly IDbConfig _DbConfig;
        private readonly IAgConfig _AgConfig;

        public AgTracingSetsConfigBuilder(IDbConfig dbConfig, IAgConfig agConfig)
        {
            _DbConfig = dbConfig;
            _AgConfig = agConfig;
        }

        public AgTracingSetsConfig Execute()
        {
            var expired = new StandardUtcDateTimeProvider().Now() - TimeSpan.FromDays(_AgConfig.TracingSetLifetimeDays);
            using (var client = new CosmosClientEx(_DbConfig))
            {
                var activeTracingSets = client.Query<AgTracingSetConfigEntity>()
                    .Where(x => x.Release > expired)
                    .Select(x => x.PublishingId)
                    .ToArray();

                return new AgTracingSetsConfig
                {
                    Ids = activeTracingSets
                };
            }
        }
    }
}
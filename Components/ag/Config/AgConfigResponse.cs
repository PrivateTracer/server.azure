﻿namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class AgConfigResponse
    {
        public AgTracingSetsConfig TracingSets { get; set; }
        public string RivmAdvice { get; set; }
        public string RssiThresholds { get; set; }
        public string TracingCalcConfig { get; set; }
    }
}
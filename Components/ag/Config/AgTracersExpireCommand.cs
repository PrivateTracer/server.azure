using System;
using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class AgTracersExpireCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _UtcDateTimeProvider;
        private readonly IAgConfig _AgConfig;

        public AgTracersExpireCommand(IDbConfig config, IUtcDateTimeProvider utcDateTimeProvider, IAgConfig agConfig)
        {
            _DbConfig = config;
            _UtcDateTimeProvider = utcDateTimeProvider;
            _AgConfig = agConfig;
        }

        public async Task Execute()
        {
            var now = _UtcDateTimeProvider.Now();

            using var c = new CosmosClientEx(_DbConfig);

            var q = c.Query<AgTracerEntity>()
                .Where(x => x.Created < now - TimeSpan.FromDays(_AgConfig.TracerLifetimeDays))
                .Select(x => new { x.Id, x.Region})
                .ToArray();

            foreach (var i in q)
            {
                await c.DeleteAsync<AgTracerEntity>(i.Id);
            }
        }
    }
}
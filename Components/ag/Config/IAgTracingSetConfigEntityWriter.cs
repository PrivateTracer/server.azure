using System.Threading.Tasks;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public interface IAgTracingSetConfigEntityWriter
    {
        Task<AgTracingSetConfigEntity> Execute(AgTracingSetConfigEntity args);
    }
}
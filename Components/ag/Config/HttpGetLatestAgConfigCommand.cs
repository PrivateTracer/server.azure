﻿using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class HttpGetLatestAgConfigCommand
    {
        private readonly AgConfigBuilder _ConfigBuilder;

        public HttpGetLatestAgConfigCommand(AgConfigBuilder configBuilder)
        {
            _ConfigBuilder = configBuilder;
        }

        public IActionResult Execute()
        {
            return new OkObjectResult(_ConfigBuilder.Execute());
        }
    }
}
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class AgTracingSetDbInsertCommand
    {
        private readonly IDbConfig _DbConfig;

        public AgTracingSetDbInsertCommand(IDbConfig config)
        {
            _DbConfig = config;
        }

        public async Task<AgTracingSetConfigEntity> Execute(AgTracingSetConfigEntity args)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return await c.InsertAsync(args);
        }
    }
}
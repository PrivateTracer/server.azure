using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    /// <summary>
    /// TODO incomplete.
    /// </summary>
    public class AgTracingSetConfigEntity : PublishedDatabaseEntity
    {
        /// <summary>
        /// Metadata
        /// </summary>
        public int Qualifier { get; set; }
        
        /// <summary>
        /// Content
        /// </summary>
        public AgTracerKey[] Items { get; set; }
    }
}
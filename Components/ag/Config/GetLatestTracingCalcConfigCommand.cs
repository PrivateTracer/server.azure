﻿using System.Linq;
using PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.Config
{
    public class GetLatestTracingCalcConfigCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _DateTimeProvider;

        public GetLatestTracingCalcConfigCommand(IDbConfig dbConfig, IUtcDateTimeProvider dateTimeProvider)
        {
            _DbConfig = dbConfig;
            _DateTimeProvider = dateTimeProvider;
        }

        public string Execute()
        {
            using var c = new CosmosClientEx(_DbConfig);
            var now = _DateTimeProvider.Now();
            return c.Query<AgTracingCalcConfigEntity>()
                .Where(x => x.Release <= now)
                .OrderByDescending(x => x.Release)
                .Take(1)
                .Select(x => x.PublishingId)
                .ToArray()
                .SingleOrDefault();
        }
    }
}
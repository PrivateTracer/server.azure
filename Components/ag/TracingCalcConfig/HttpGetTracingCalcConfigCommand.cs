using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{
    
    public class HttpGetTracingCalcConfigCommand
    {
        private readonly SafeGetTracingCalcConfigCommand _SafeReadcommand;

        public HttpGetTracingCalcConfigCommand(SafeGetTracingCalcConfigCommand safeReadcommand)
        {
            _SafeReadcommand = safeReadcommand;
        }

        public IActionResult Execute(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return new BadRequestResult();

            var e = _SafeReadcommand.Execute(id.Trim());

            if (e == null)
                return new NotFoundResult();

            var result = new TracingCalcConfigResponse
            {
                MinimumRiskScore = e.MinimumRiskScore,
                Attenuation = new WeightingResponse { Weight = e.Attenuation.Weight, LevelValues = e.Attenuation.LevelValues },
                DaysSinceLastExposure = new WeightingResponse { Weight = e.DaysSinceLastExposure.Weight, LevelValues = e.DaysSinceLastExposure.LevelValues },
                DurationLevelValues = new WeightingResponse { Weight = e.DurationLevelValues.Weight, LevelValues = e.DurationLevelValues.LevelValues },
                TransmissionRisk = new WeightingResponse { Weight = e.TransmissionRisk.Weight, LevelValues = e.TransmissionRisk.LevelValues },
            };

            return new OkObjectResult(result);
        }
    }
}
using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{
    public class SafeGetTracingCalcConfigCommand
    {
        private readonly IDbConfig _DbConfig;

        public SafeGetTracingCalcConfigCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public AgTracingCalcConfigEntity Execute(string id)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return c.Query<AgTracingCalcConfigEntity>()
                .Where(x => x.PublishingId == id)
                .Take(1)
                .ToArray()
                .SingleOrDefault();
        }
    }
}
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{
    public class AgTracingCalcConfigEntity : PublishedDatabaseEntity
    {
        public int MinimumRiskScore { get; set; }
        public Weighting Attenuation { get; set; }
        public Weighting DaysSinceLastExposure { get; set; }
        public Weighting DurationLevelValues { get; set; }
        public Weighting TransmissionRisk { get; set; }
    }
}
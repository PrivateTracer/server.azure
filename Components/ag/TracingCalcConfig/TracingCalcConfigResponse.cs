﻿namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{

    public class WeightingResponse
    {
        public int Weight { get; set; }
        public int[] LevelValues { get; set; }//:[1, 2, 3, 4, 5, 6, 7, 8],
    }
    public class Weighting
    {
        public int Weight { get; set; }
        public int[] LevelValues { get; set; }//:[1, 2, 3, 4, 5, 6, 7, 8],
    }

    public class TracingCalcConfigResponse
    {
        public int MinimumRiskScore { get; set; }
        public WeightingResponse Attenuation { get; set; } 
        public WeightingResponse DaysSinceLastExposure { get; set; }
        public WeightingResponse DurationLevelValues { get; set; } 
        public WeightingResponse TransmissionRisk { get; set; }
    }
}
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{
    public class HttpPostTracingCalcConfigCommand
    {
        private readonly TracingCalcConfigInsertDbCommand _Writer;
        private readonly TracingCalcConfigValidator _Validator;

        public HttpPostTracingCalcConfigCommand(TracingCalcConfigInsertDbCommand writer, TracingCalcConfigValidator validator)
        {
            _Writer = writer;
            _Validator = validator;
        }

        public async Task<IActionResult> Execute(TracingCalcConfigArgs args)
        {
            if (!_Validator.Valid(args))
                return new BadRequestResult();

            await _Writer.Execute(args);
            return new OkResult();
        }
    }
}
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{
    public class TracingCalcConfigInsertDbCommand
    {
        private readonly IDbConfig _DbConfig;

        public TracingCalcConfigInsertDbCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public async Task Execute(TracingCalcConfigArgs args)
        {
            var e = new AgTracingCalcConfigEntity
            {
                MinimumRiskScore = args.MinimumRiskScore,
                Attenuation = new Weighting { Weight = args.Attenuation.Weight, LevelValues = args.Attenuation.LevelValues },
                DaysSinceLastExposure = new Weighting { Weight = args.DaysSinceLastExposure.Weight, LevelValues = args.DaysSinceLastExposure.LevelValues },
                DurationLevelValues = new Weighting { Weight = args.DurationLevelValues.Weight, LevelValues = args.DurationLevelValues.LevelValues },
                TransmissionRisk = new Weighting { Weight = args.TransmissionRisk.Weight, LevelValues = args.TransmissionRisk.LevelValues },
                Release = args.Release,
            };
            using var client = new CosmosClientEx(_DbConfig);
            await client.InsertAsync(e);
        }
    }
}
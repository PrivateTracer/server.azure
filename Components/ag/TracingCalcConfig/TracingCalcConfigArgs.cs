﻿using System;

namespace PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig
{
    public class TracingCalcConfigArgs
    {
        public int MinimumRiskScore { get; set; }
        public Weighting Attenuation { get; set; }
        public Weighting DaysSinceLastExposure { get; set; }
        public Weighting DurationLevelValues { get; set; }
        public Weighting TransmissionRisk { get; set; }
        public DateTime Release { get; set; }
    }
}
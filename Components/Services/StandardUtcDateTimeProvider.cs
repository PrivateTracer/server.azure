﻿using System;

namespace PrivateTracer.Server.Azure.Components.Services
{
    public class StandardUtcDateTimeProvider : IUtcDateTimeProvider
    {
        public DateTime Now() => DateTime.UtcNow;
    }
}
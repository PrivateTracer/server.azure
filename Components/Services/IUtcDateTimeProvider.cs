using System;

namespace PrivateTracer.Server.Azure.Components.Services
{
    public interface IUtcDateTimeProvider
    {
        DateTime Now();
    }
}
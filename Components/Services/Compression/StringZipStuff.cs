﻿using System.IO;
using System.IO.Compression;

namespace PrivateTracer.Server.Azure.Components.Services
{
    public static class StringZipStuff
    {
        public static byte[] Zip(this byte[] bytes)
        {
            using (var output = new MemoryStream())
            {
                using (var input = new MemoryStream(bytes))
                {
                    using (var zipper = new GZipStream(output, CompressionLevel.Optimal))
                    {
                        input.CopyTo(zipper);
                    }
                }
                return output.ToArray();
            }
        }

        /// <summary>
        /// Feed the result of this function into an encoding GetString:
        /// E.g. Encoding.Utf8.GetString(unzippedArray); 
        /// </summary>
        public static byte[] Unzip(this byte[] bytes)
        {
            using (var input = new MemoryStream(bytes))
            using (var output = new MemoryStream())
            using (var zipper = new GZipStream(input, CompressionMode.Decompress))
            {
                zipper.CopyTo(output);
                return output.ToArray();
            }
        }
    }
}

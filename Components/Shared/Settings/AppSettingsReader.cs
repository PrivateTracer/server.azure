using System;
using Microsoft.Extensions.Configuration;

namespace PrivateTracer.Server.Azure.ServerLite.Components
{
    public abstract class AppSettingsReader
    {
        private readonly IConfiguration _Config;

        protected virtual string Prefix { get; }

        protected AppSettingsReader(IConfiguration config, string? prefix = null)
        {
            _Config = config;

            if (string.IsNullOrWhiteSpace(prefix) || prefix != prefix.Trim())
                Prefix = string.Empty;
            else
                Prefix = prefix + ":";
        }

        protected int GetValueInt32(string path, int defaultValue = int.MinValue)
        {
            var found = _Config[$"{Prefix}{path}"];
            return string.IsNullOrWhiteSpace(found) ? defaultValue : Convert.ToInt32(found);
        }

        protected string GetValue(string path, string defaultValue = "Unspecified default")
        {
            var found = _Config[$"{Prefix}{path}"];
            return string.IsNullOrWhiteSpace(found) ? defaultValue : found;
        }
    }
}
using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.Components.Settings;

namespace PrivateTracer.Server.Azure.Components.Services
{
    public static class AppSettingsFromJsonFiles
    {
        /// <summary>
        /// Just for command line apps
        /// </summary>
        /// <returns></returns>
        public static IDbConfig CreateDbConfig() 
            => new DbConfigAppSettings(GetConfigurationRoot(), DatabaseConfigNames.Main);

        private static IConfigurationRoot GetConfigurationRoot()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false, false)
                .AddJsonFile("appsettings.Development.json", true, false)
                .Build();
            return config;
        }
    }
}
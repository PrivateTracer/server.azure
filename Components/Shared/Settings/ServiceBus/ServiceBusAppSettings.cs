using System;
using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.Services
{
    public class ServiceBusAppSettings : AppSettingsReader, IServiceBusConfig
    {
        public ServiceBusAppSettings(IConfiguration config, string? prefix = null) : base(config, prefix) { }
        public string QueueName => GetValue(nameof(QueueName));
        public string ConnectionString => GetValue(nameof(ConnectionString));
    }
}
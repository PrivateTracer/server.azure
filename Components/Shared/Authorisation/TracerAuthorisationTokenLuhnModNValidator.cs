﻿using System;

namespace PrivateTracer.Server.Azure.Components.Services
{
    public class TracerAuthorisationTokenLuhnModNValidator : ITracerAuthorisationTokenValidator
    {
        private readonly LuhnModNValidator _Validator;

        public TracerAuthorisationTokenLuhnModNValidator(ILuhnModNConfig config)
        {
            _Validator = new LuhnModNValidator(config);
        }

        public bool IsValid(string? value) => _Validator.Validate(value);
    }
}
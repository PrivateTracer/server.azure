﻿namespace PrivateTracer.Server.Azure.Components.Services
{
    public interface ILuhnModNConfig
    {
        char[] CharacterSet { get; }
        int ValueLength { get; }
    }
}
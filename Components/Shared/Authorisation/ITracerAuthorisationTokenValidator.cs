﻿namespace PrivateTracer.Server.Azure.Components.Services
{
    public interface ITracerAuthorisationTokenValidator
    {
        bool IsValid(string? token);
    }
}
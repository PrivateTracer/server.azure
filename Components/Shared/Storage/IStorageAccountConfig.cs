namespace PrivateTracer.Server.Azure.Components.Settings
{
    public interface IStorageAccountConfig
    {
        string ConnectionString { get; }
    }
}
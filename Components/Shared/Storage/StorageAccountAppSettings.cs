using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.Settings
{
    public class StorageAccountAppSettings : AppSettingsReader, IStorageAccountConfig
    {
        public StorageAccountAppSettings(IConfiguration config, string prefix) : base(config, prefix)
        {
        }

        public string ConnectionString => GetValue(nameof(ConnectionString));

        public string Uri => GetValue(nameof(Uri));
        
        ////TODO integrate secrets vault
        public string Token => GetValue(nameof(Token));
    }
}
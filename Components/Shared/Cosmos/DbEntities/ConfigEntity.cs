using System;

namespace PrivateTracer.Server.Azure.Components.DbEntities
{
    public class ConfigEntity : DatabaseEntity
    {
        public string Content { get; set; }
        public DateTime Created { get; set; }
    }
}
using System;

namespace PrivateTracer.Server.Azure.Components.DbEntities
{
    public abstract class PublishedDatabaseEntity : DatabaseEntity
    {
        public string PublishingId { get; set; }
        public DateTime Release { get; set; }
        public override void SetKeys()
        {
            base.SetKeys();
            PublishingId = Id.GetHashCode().ToString("X");
        }
    }
}
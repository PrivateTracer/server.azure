using System;
using Newtonsoft.Json;

namespace PrivateTracer.Server.Azure.Components.DbEntities
{
    public abstract class DatabaseEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        public string Region { get; set; } = DefaultValues.Region;
        public virtual void SetKeys()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
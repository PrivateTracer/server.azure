using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.RivmAdvice;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;

namespace PrivateTracer.Server.Azure.Components.Services
{

    /// <summary>
    /// TODO guards for disposal
    /// </summary>
    public sealed class CosmosClientEx : IDisposable
    {
        /// <summary>
        /// Used in CosmosClientEx when a database id is given at run-time along with an already connected CosmosClient
        /// </summary>
        private class DbIdOnlyConfig : IDbConfig
        {
            public DbIdOnlyConfig(string databaseId)
            {
                Id = databaseId;
            }

            public string Uri => throw new NotImplementedException();
            public string Token => throw new NotImplementedException();
            public string Id { get; }
        }

        public string GetContainerName<T>() where T : DatabaseEntity
            => typeof(T).Name.Substring(0, typeof(T).Name.Length - 6);

        private readonly IDbConfig _Config;
        private readonly CosmosClient _Client;

        public CosmosClientEx(IDbConfig config)
        {
            _Config = config;
            _Client = new CosmosClient(config.Uri, config.Token);
        }

        public CosmosClientEx(CosmosClient client, string databaseId)
        {
            _Config = new DbIdOnlyConfig(databaseId);
            _Client = client;
        }

        private Container GetContainer<T>() where T : DatabaseEntity
            => _Client.GetContainer(_Config.Id, GetContainerName<T>());

        public async Task CreateDatabaseIfNotExistsAsync()
            => await _Client.CreateDatabaseIfNotExistsAsync(_Config.Id);

        public async Task DeleteDatabaseAsnyc() {
            //TODO null ref exception - timing?
            }
            //=> await _Client.GetDatabase(_Config.DatabaseId).DeleteAsync();

        public async Task CreateContainerIfNotExistsAsync<T>() where T : DatabaseEntity
            => await _Client.GetDatabase(_Config.Id)
                .CreateContainerIfNotExistsAsync(GetContainerName<T>(), $"/Region");

        public async Task<T> InsertAsync<T>(T entity) where T : DatabaseEntity
        {
            var tryCounter = 3;
            while (!await TryInsert(GetContainer<T>(), entity) && tryCounter > 0)
                tryCounter--;

            if (tryCounter == 0)
                throw new OutOfCheeseException("Entity write failed after several attempts.");

            return entity;
        }

        private static async Task<bool> TryInsert<T>(Container c, T entity) where T : DatabaseEntity
        {
            try
            {
                entity.SetKeys();
                await c.CreateItemAsync(entity);
                return true;
            }
            catch (CosmosException e)
            {
                if (e.StatusCode != HttpStatusCode.Conflict) 
                    throw;
                
                return false;
            }
        }

        public IOrderedQueryable<T> Query<T>() where T : DatabaseEntity
            => GetContainer<T>().GetItemLinqQueryable<T>(true);

        public async Task UdpateAsync<T>(T entity) where T : DatabaseEntity
            => await GetContainer<T>().ReplaceItemAsync(entity, entity.Id);

        public async Task DeleteAsync<T>(T e) where T : DatabaseEntity
            => await DeleteAsync<T>(e.Id);

        public async Task DeleteAsync<T>(string id) where T : DatabaseEntity
            => await GetContainer<T>().DeleteItemAsync<T>(id, new PartitionKey("NL")); //HACK

        public void Dispose()
        {
            _Client?.Dispose();
        }
    }
}
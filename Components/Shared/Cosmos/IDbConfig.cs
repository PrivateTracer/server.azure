namespace PrivateTracer.Server.Azure.Components.Services
{
    public interface IDbConfig
    {
        string Uri { get; }
        string Token { get; }
        string Id { get; }
    }
}
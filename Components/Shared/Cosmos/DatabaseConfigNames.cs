namespace PrivateTracer.Server.Azure.Components.Services
{
    public static class DatabaseConfigNames
    {
        public const string Main = "Database:Main";
        public const string TracingSetsEngine = "Database:TSE";
        public const string ConfigEngine = "Database:Config";
    }

    public static class StorageAccoungConfigNames
    {
        public const string Cdn = "Storage:CDN";
    }
}
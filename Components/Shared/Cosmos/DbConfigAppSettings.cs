using Microsoft.Extensions.Configuration;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.Components.Settings
{
    /// <summary>
    /// NB this does not support prefixes as only TracingSetEngine requires 2 dbs which is deployed as an Function App which use Environment Variables
    /// </summary>
    public class DbConfigAppSettings : AppSettingsReader, IDbConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <param name="prefix"></param>
        public DbConfigAppSettings(IConfiguration config, string prefix) : base(config, prefix)
        {
        }

        public string Uri => GetValue(nameof(Uri));
        public string Token => GetValue(nameof(Token));
        public string Id => GetValue(nameof(Id));
    }
}
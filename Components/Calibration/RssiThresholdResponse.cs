﻿namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class RssiThresholdResponse
    {
        public double Default { get; set; }
        public DeviceValue[] Devices { get; set; }
    }
}
﻿using System;

namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class RssiThresholdArgs
    {

        public double Default { get; set; }
        public DeviceValueArgs[] Devices { get; set; }
        public DateTime Release { get; set; }
    }
}
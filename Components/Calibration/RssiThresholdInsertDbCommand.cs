using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class RssiThresholdInsertDbCommand
    {
        private readonly IDbConfig _DbConfig;

        public RssiThresholdInsertDbCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public async Task Execute(RssiThresholdArgs args)
        {
            var e = new RssiThresholdConfigEntity
            {
                Release = args.Release,
                Default = args.Default,
                Devices = args.Devices.Select(x => new DeviceValue { Id = x.Id, Value = x.Value}).ToArray()
            };
            using var client = new CosmosClientEx(_DbConfig);
            await client.InsertAsync(e);
        }
    }
}
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class RssiThresholdConfigEntity: PublishedDatabaseEntity
    {
        public double Default { get; set; }
        public DeviceValue[] Devices { get; set; }
    }
}
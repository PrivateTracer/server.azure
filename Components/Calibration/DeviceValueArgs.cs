﻿namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class DeviceValueArgs
    {
        public string Id { get; set; }
        public double Value { get; set; }
    }
}
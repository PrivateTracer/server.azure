using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class HttpGetRssiThresholdsCommand
    {
        private readonly SafeGetRssiThresholdsCommand _SafeReadcommand;

        public HttpGetRssiThresholdsCommand(SafeGetRssiThresholdsCommand safeReadcommand)
        {
            _SafeReadcommand = safeReadcommand;
        }

        public IActionResult Execute(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return new BadRequestResult();

            var e = _SafeReadcommand.Execute(id.Trim());

            if (e == null)
                return new NotFoundResult();

            var result = new RssiThresholdResponse
            {
                Default = e.Default,
                Devices = e.Devices
            };

            return new OkObjectResult(result);
        }
    }
}
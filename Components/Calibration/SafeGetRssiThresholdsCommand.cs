using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class SafeGetRssiThresholdsCommand
    {
        private readonly IDbConfig _DbConfig;

        public SafeGetRssiThresholdsCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public RssiThresholdConfigEntity Execute(string id)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return c.Query<RssiThresholdConfigEntity>()
                .Where(x => x.PublishingId == id)
                .Take(1)
                .ToArray()
                .SingleOrDefault();
        }
    }
}
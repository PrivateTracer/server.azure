using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class HttpPostRssiThresholdCommand
    {
        private readonly RssiThresholdInsertDbCommand _Writer;
        private readonly RssiThresholdValidator _Validator;

        public HttpPostRssiThresholdCommand(RssiThresholdInsertDbCommand writer, RssiThresholdValidator validator)
        {
            _Writer = writer;
            _Validator = validator;
        }

        public IActionResult Execute(RssiThresholdArgs args)
        {
            if (!_Validator.Valid(args))
                return new BadRequestResult();

            _Writer.Execute(args);
            return new OkResult();
        }
    }
}
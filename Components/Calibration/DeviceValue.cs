﻿namespace PrivateTracer.Server.Azure.Components.Calibration
{
    public class DeviceValue
    {
        public string Id { get; set; }
        public double Value { get; set; }
    }
}
﻿using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class GetLatestRivmAdviceCommand
    {
        private readonly IDbConfig _DbConfig;
        private readonly IUtcDateTimeProvider _DateTimeProvider;

        public GetLatestRivmAdviceCommand(IDbConfig dbConfig, IUtcDateTimeProvider dateTimeProvider)
        {
            _DbConfig = dbConfig;
            _DateTimeProvider = dateTimeProvider;
        }

        public string Execute()
        {
            using var c = new CosmosClientEx(_DbConfig);
            var now = _DateTimeProvider.Now();
            return c.Query<RivmAdviceConfigEntity>()
                .Where(x => x.Release <= now)
                .OrderByDescending(x => x.Release)
                .Take(1)
                .Select(x => x.PublishingId)
                .ToArray()
                .SingleOrDefault();
        }
    }
}
﻿using System;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class MobileDeviceRivmAdviceArgs
    {
        public DateTime Release { get; set; }
        public int TracingKeyRetentionDays { get; set; }
        public int ObservedTracingKeyRetentionDays { get; set; }
        public int IsolationPeriodDays { get; set; }
        public LocalizableTextArgs[] Text { get; set; }
    }
}
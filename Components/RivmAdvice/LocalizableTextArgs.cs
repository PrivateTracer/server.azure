﻿namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class LocalizableTextArgs
    {
        /// <summary>
        /// E.g. nl-NL, nl-BE, en, fr-BE?
        /// See https://docs.microsoft.com/en-us/dotnet/api/system.globalization.cultureinfo.name?view=netcore-3.1
        /// </summary>
        public string Locale { get; set; }

        /// <summary>
        /// Base64
        /// Then Moustache...
        ///// </summary>
        public string IsolationAdviceShort { get; set; }

        /// <summary>
        /// Base64
        /// Then Moustache...
        ///// </summary>
        public string IsolationAdviceLong { get; set; }
    }
}
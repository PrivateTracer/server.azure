namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class HardCodedRivmAdviceValidationConfig
    {
        public string[] Locales { get; } = {"en", "nl-NL"};
    }
}
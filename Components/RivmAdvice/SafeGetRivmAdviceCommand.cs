﻿using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class SafeGetRivmAdviceCommand
    {
        private readonly IDbConfig _DbConfig;

        public SafeGetRivmAdviceCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public RivmAdviceConfigEntity Execute(string id)
        {
            using var c = new CosmosClientEx(_DbConfig);
            return c.Query<RivmAdviceConfigEntity>()
                .Where(x => x.PublishingId == id)
                .Take(1)
                .ToArray()
                .SingleOrDefault();
        }
    }
}
﻿using System;
using PrivateTracer.Server.Azure.Components.DbEntities;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class RivmAdviceConfigEntity : PublishedDatabaseEntity
    {
        public int TracingKeyRetentionDays { get; set; }
        public int ObservedTracingKeyRetentionDays { get; set; }
        public int IsolationPeriodDays { get; set; }
        public LocalizableText[] Text { get; set; }
    }
}
using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class HttpPostRivmAdviceCommand
    {
        private readonly RivmAdviceInsertDbCommand _Writer;
        private readonly RivmAdviceValidator _Validator;

        public HttpPostRivmAdviceCommand(RivmAdviceInsertDbCommand writer, RivmAdviceValidator validator)
        {
            _Writer = writer;
            _Validator = validator;
        }

        public IActionResult Execute(MobileDeviceRivmAdviceArgs args)
        {
            if (!_Validator.Valid(args))
                return new BadRequestResult();

            _Writer.Execute(args);
            return new OkResult();
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class HttpGetRivmAdviceCommand
    {
        private readonly SafeGetRivmAdviceCommand _SafeReadcommand;

        public HttpGetRivmAdviceCommand(SafeGetRivmAdviceCommand safeReadcommand)
        {
            _SafeReadcommand = safeReadcommand;
        }

        public IActionResult Execute(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return new BadRequestResult();

            var e = _SafeReadcommand.Execute(id.Trim());

            if (e == null)
                return new NotFoundResult();

            var result = new RivmAdviceResponse
            {
                Id = id,
                IsolationPeriodDays = e.IsolationPeriodDays,
                TracingKeyRetentionDays = e.TracingKeyRetentionDays,
                ObservedTracingKeyRetentionDays = e.ObservedTracingKeyRetentionDays,
                Text = e.Text
            };

            return new OkObjectResult(result);
        }
    }
}
﻿namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class RivmAdviceResponse
    {
        public string Id { get; set; }
        public int TracingKeyRetentionDays { get; set; }
        public int ObservedTracingKeyRetentionDays { get; set; }
        public int IsolationPeriodDays { get; set; }
        public LocalizableText[] Text { get; set; }
    }
}
using System;
using System.Linq;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.Components.RivmAdvice
{
    public class RivmAdviceInsertDbCommand
    {
        private readonly IDbConfig _DbConfig;

        public RivmAdviceInsertDbCommand(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public async Task Execute(MobileDeviceRivmAdviceArgs args)
        {
            var e = new RivmAdviceConfigEntity
            {
                Release = args.Release,
                Text = args.Text.Select(x => new LocalizableText {
                    Locale = x.Locale, 
                    IsolationAdviceLong = x.IsolationAdviceLong, 
                    IsolationAdviceShort = x.IsolationAdviceShort
                }).ToArray(),
                IsolationPeriodDays = args.IsolationPeriodDays,
                ObservedTracingKeyRetentionDays = args.ObservedTracingKeyRetentionDays,
                TracingKeyRetentionDays = args.TracingKeyRetentionDays,
            };
            using var client = new CosmosClientEx(_DbConfig);
            await client.InsertAsync(e);
        }
    }
}
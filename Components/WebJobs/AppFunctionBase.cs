﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.Components.WebJobs
{
    public abstract class AppFunctionBase
    {
        protected IConfigurationRoot Configuration { get; private set; }

        protected void SetConfig(ExecutionContext context)
        {
            if (Configuration != null)
                return;

            Configuration = new ConfigurationBuilder()
                .SetBasePath(context.FunctionAppDirectory)
                .AddJsonFile("local.settings.json", true)
                .AddEnvironmentVariables()
                .Build();
        }

        public void LogConfig(ILogger log, ExecutionContext context)
        {
            SetConfig(context);

            var c0 = new ServiceBusAppSettings(Configuration, "ServiceBus:TracerAuths");
            log.LogInformation($"ServiceBus:TracerAuths:ConnectionString - {c0.ConnectionString}");
            log.LogInformation($"ServiceBus:TracerAuths:QueueName - {c0.QueueName}");

            var c0a = new ServiceBusAppSettings(Configuration, "ServiceBus:Tracers");
            log.LogInformation($"ServiceBus:Tracers:ConnectionString - {c0a.ConnectionString}");
            log.LogInformation($"ServiceBus:Tracers:QueueName - {c0a.QueueName}");

            var c1 = new TracerValidationAppSettings(Configuration);
            log.LogInformation($"Validation:Tracers:ItemCountMin - {c1.TracerKeyCountMin}");
            log.LogInformation($"Validation:Tracers:ItemCountMax - {c1.TracerKeyCountMax}");

            var c2 = new TracerKeyValidatorAppSettings(Configuration);
            log.LogInformation($"Validation:TracerKey:Epoch:Min - {c2.EpochMin}");
            log.LogInformation($"Validation:TracerKey:Epoch:Max - {c2.EpochMax}");
            log.LogInformation($"Validation:TracerKey:KeyByteCount - {c2.KeyByteCount}");
        }
    }
}

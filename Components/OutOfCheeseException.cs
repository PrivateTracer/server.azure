using System;
using System.Runtime.Serialization;

namespace PrivateTracer.Server.Azure.Components
{
    public class OutOfCheeseException : Exception
    {
        [Obsolete("Use ctor(string)")]
        public OutOfCheeseException()
        {
        }

        public OutOfCheeseException(string message) : base(message) { }

        public OutOfCheeseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OutOfCheeseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
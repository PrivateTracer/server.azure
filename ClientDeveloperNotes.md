# Client Developer Notes

* POST Tracer now expects a valid 10 character Authorisation Token
* POST Tacer now return 200 rather than 204.
* GET Config now returns the current version of all configurable items including a list of Tracing Sets.
* For RIVM advice, if the Id is different to the locally stored Id, GET the new one.
* Similarly for Tracing Sets, GET the ones the client has not yet seen. Record the Ids that are succesfully processed. Remove the Ids from the local 'processed' list if they are no longer present in the list in the config.
* INFO only - All GET identifiers are the hex representation of a 32bit integer (cos -ve numbers in the URL trigger my OCD :-) )
* NB Next version of the DP3T Rust Library will encode the Tracing Set in Base64. This will remove any confusion regarding how it is to be serialised in JSON.

# Updating the client data


Clients GET config and then GET the individual parts they need to update compared with what they have on the mobile device.

Polling is currently assumed to take place when the device wakes or every hour after some random interval or similar. Also this will help spread the load on the server.

Item id's to be simple 4byte hashes of the content in hex representation. An example message would look like:

```
{
"TracingSets": {"BFAA8485", "123445AD"}, 
"RivmAdvice": "11243DE",
"Anything else we think of":"11112222"
}
```

For each tracing set:

* If the device has the id in the local list, no action.
* If the device does not have the id in the local list, it records the id in the local list, GETS the document and processes it.
* If the device has the id but the /config response does not, delete the id from the local list.

For the RivmAdvice, just remember one id. If the id is different to the one seen before, GET it, process it and overwrite the remembered id.

The individual documents are fetched from the given path by appending the version number e.g.: `/tracingsets/<id>` or `/rivmadvice/<id>`


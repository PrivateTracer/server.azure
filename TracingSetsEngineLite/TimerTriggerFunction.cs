using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace TracingSetsEngineLite
{
    public static class TimerTriggerFunction
    {
        private static IConfigurationRoot Configuration { get; }

        static TimerTriggerFunction()
        {
            var builder = new ConfigurationBuilder();
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        [FunctionName("TracingSetsEngineLite_TimerTrigger")]
        public static void Run([TimerTrigger("0 */2 * * * *")]TimerInfo myTimer, ILogger log)
        {
            var source = new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main);
            var job = new DbConfigAppSettings(Configuration, DatabaseConfigNames.TracingSetsEngine);
            using var bb = new BuildCuckooFiltersBatchJob(
                source,
                job, //DB Name is generated but same account allowed.
                new StandardUtcDateTimeProvider(), new TracingSetMessageDbSender(new TracingSetDbInsertCommand(source))
            );
        }

        [FunctionName("AgTracingSetsEngineLite_TimerTrigger")]
        public static void RunAg([TimerTrigger("0 */2 * * * *")]TimerInfo myTimer, ILogger log)
        {
            var source = new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main);
            var job = new DbConfigAppSettings(Configuration, DatabaseConfigNames.TracingSetsEngine);
            using var bb = new BuildAgTracingSetBatchJob(
                source,
                job, //DB Name is generated but same account allowed.
                new StandardUtcDateTimeProvider(), 
                new AgTracingSetMessageDbSender(new AgTracingSetDbInsertCommand(source)), 
                new AgConfigAppSettings(Configuration)
            );
        }
    }
}

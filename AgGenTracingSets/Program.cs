﻿using System;
using PrivateTracer.Server.Azure.Components.ag;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.ag.TracingSetsEngine;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;
using PrivateTracer.Server.Azure.DevServer.DataUtilities.Components;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.GenTracingSets
{

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var db = AppSettingsFromJsonFiles.CreateDbConfig();

                using var bb = new BuildAgTracingSetBatchJob
                (
                    db,
                    db, //DB Name is generated but same account allowed.
                    new StandardUtcDateTimeProvider(), 
                    new AgTracingSetMessageDbSender(new AgTracingSetDbInsertCommand(db)),
                    new HardCodedAgConfig()
                );
                bb.Execute().GetAwaiter().GetResult();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
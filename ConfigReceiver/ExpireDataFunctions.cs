using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace ConfigReceiver
{
    public static class ExpireDataFunctions
    {
        private static IConfigurationRoot Configuration { get; }

        static ExpireDataFunctions()
        {
            var builder = new ConfigurationBuilder();
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        [FunctionName("Tracers_Expire")]
        public static async Task RunTracersExpire([TimerTrigger("0 */300 * * * *")] TimerInfo timerInfo, ILogger __)
        {
            await new TracersExpireCommand(new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main), new StandardUtcDateTimeProvider(), new Dp3tAppSettings(Configuration))
                .Execute();
        }

        [FunctionName("TracingSets_Expire")]
        public static async Task RunTracingSetsExpire([TimerTrigger("0 */300 * * * *")] TimerInfo timerInfo, ILogger __)
        {
            var dbConfig = new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main);
            await new TracingSetExpireCommand(dbConfig, new StandardUtcDateTimeProvider(), new Dp3tAppSettings(Configuration), new StorageAccountAppSettings(Configuration, "CDN"))
                .Execute();
        }
    }
}
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;

namespace ConfigReceiver
{
    public class UpdateConfigBatchJob
    {
        private readonly ConfigBuilder _ConfigBuilder;
        private readonly ConfigBlobWriter _BlobWriter;
        private readonly IDbConfig _DbConfig;
        private readonly StandardUtcDateTimeProvider _UtcDateTimeProvider;

        public UpdateConfigBatchJob(ConfigBuilder configBuilder, ConfigBlobWriter blobWriter, IDbConfig dbConfig, StandardUtcDateTimeProvider utcDateTimeProvider)
        {
            _ConfigBuilder = configBuilder;
            _BlobWriter = blobWriter;
            _DbConfig = dbConfig;
            _UtcDateTimeProvider = utcDateTimeProvider;
        }

        public async Task Execute()
        {
            var config = _ConfigBuilder.Execute();
            var json = JsonConvert.SerializeObject(config);

            using var client = new CosmosClientEx(_DbConfig);
            var currentConfig = client.Query<ConfigEntity>().Take(1).ToArray().SingleOrDefault();

            if (currentConfig?.Content != json)
            {
                await client.InsertAsync(new ConfigEntity {Content = json, Created = _UtcDateTimeProvider.Now()});
                _BlobWriter.Execute(config);
            }
        }
    }
}
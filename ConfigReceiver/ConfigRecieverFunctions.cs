using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.Calibration;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.pt.Mapping;
using PrivateTracer.Server.Azure.Components.RivmAdvice;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;
using PrivateTracer.Server.Azure.Components.WebJobs;

namespace ConfigReceiver
{
    public class ConfigRecieverFunctions : AppFunctionBase
    {
        [FunctionName("RivmAdvice_Post")]
        public async Task<IActionResult> PostRivmAdvice(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]
            HttpRequest req,
            ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var args = JsonConvert.DeserializeObject<MobileDeviceRivmAdviceArgs>(requestBody);

            var handler = new HttpPostRivmAdviceCommand(
                new RivmAdviceInsertDbCommand(new DbConfigAppSettings(Configuration, DatabaseConfigNames.ConfigEngine)),
                new RivmAdviceValidator()
            );

            return handler.Execute(args);
        }

        [FunctionName("RssiThreshold_Post")]
        public async Task<IActionResult> PostRssiThreshold(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]
            HttpRequest req,
            ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var args = JsonConvert.DeserializeObject<RssiThresholdArgs>(requestBody);

            var handler = new HttpPostRssiThresholdCommand(
                new RssiThresholdInsertDbCommand(new DbConfigAppSettings(Configuration, DatabaseConfigNames.ConfigEngine)),
                new RssiThresholdValidator()
            );

            return handler.Execute(args);
        }

        [FunctionName("TracingSets_QueueReceiver")]
        public async Task TracingSets_QueueReceiver([ServiceBusTrigger("tracingsets", Connection = "TracingSetsQueueConnectionString")]Message message, ILogger log, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var json = Encoding.UTF8.GetString(message.Body);
            var args = JsonConvert.DeserializeObject<TracingSetMessage>(json);
            var e = args.ToDbEntity();

            var dbWriter = new TracingSetDbInsertCommand(new DbConfigAppSettings(Configuration, DatabaseConfigNames.ConfigEngine));
            await dbWriter.Execute(e);
            
            var blobWriter = new TracingSetBlobWriter(new StorageAccountAppSettings(Configuration, StorageAccoungConfigNames.Cdn));
            blobWriter.Execute(e);
        }
    }

}

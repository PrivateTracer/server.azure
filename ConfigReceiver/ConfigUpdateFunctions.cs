using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.RivmAdvice;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.WebJobs;
using PrivateTracer.Server.Azure.ServerLite.Components;

namespace ConfigReceiver
{
    public class ConfigUpdateFunctions : AppFunctionBase
    {
        [FunctionName("UpdateConfig_TimerTrigger")]
        public async Task RunConfigUpdate([TimerTrigger("0 */60 * * * *"/*, RunOnStartup = true*/)] TimerInfo ti, ILogger __, ExecutionContext executionContext)
        {
            SetConfig(executionContext);
            var dbConfig = new DbConfigAppSettings(Configuration, DatabaseConfigNames.ConfigEngine);
            var dtp = new StandardUtcDateTimeProvider();
            await new UpdateConfigBatchJob(
                    new ConfigBuilder(
                        new TracingSetsConfigBuilder(dbConfig, new Dp3tAppSettings(Configuration)), 
                        new GetLatestRivmAdviceCommand(dbConfig, dtp), 
                        new GetLatestRssiThresholdsCommand(dbConfig, dtp)),
                        new ConfigBlobWriter(new StorageAccountAppSettings(Configuration, "Storage:CDN")),
                    dbConfig,
                    dtp)
                .Execute();
        }
    }
}
﻿using System;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.DevServer.DataUtilities.Components;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.GenTracers
{

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var tracerCount = Convert.ToInt32(args[0]);
                if (tracerCount < 1)
                    throw new ArgumentOutOfRangeException(nameof(tracerCount));
                
                var randomSeed = Convert.ToInt32(args[1]);
                
                var r = new Random(randomSeed); //Don't need the crypto version.
                var c1 = new GenerateTracers(AppSettingsFromJsonFiles.CreateDbConfig());
                c1.Execute(tracerCount, x => r.Next(x), x => r.NextBytes(x));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}

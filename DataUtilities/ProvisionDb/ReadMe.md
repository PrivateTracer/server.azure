# Provision Database

This adds the tables and some example data for RIVM Advice.

1. Download dotnet SDK: https://dotnet.microsoft.com/download/dotnet-core/3.1
1. Clone this repo
1. Choose your database - local emulator or Azure account
1. Add an `appsettings.Development.json` file. This overrides the settings in appsettings.json. Leave it empty if if using the local Cosmos DB emulator (the compiler expects this file to be present but it is deliberately excluded as it contains secrets), otherwise add the Uri, Token and Id settings you are given by the Server Admins.
1. Run provisiondb from the command line without building the project : dotnet run nameofdatabase


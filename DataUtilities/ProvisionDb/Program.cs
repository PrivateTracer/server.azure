﻿using System;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.DevServer.DataUtilities.Components;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.ProvisionDb
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var dpProvision = new CreateDatabaseAndCollections(AppSettingsFromJsonFiles.CreateDbConfig());
                dpProvision.Execute().GetAwaiter().GetResult();
                Console.WriteLine("Completed.");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}

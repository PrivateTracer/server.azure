﻿using System;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracingSetsEngine;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.GenTracingSets
{

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var db = AppSettingsFromJsonFiles.CreateDbConfig();

                using var bb = new BuildCuckooFiltersBatchJob(
                    db,
                    db, //DB Name is generated but same account allowed.
                    new StandardUtcDateTimeProvider(), new TracingSetMessageDbSender(new TracingSetDbInsertCommand(db))
                    );
                bb.Execute().GetAwaiter().GetResult();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}

﻿using System;
using System.Linq;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class GenerateAuthorisations
    {
        private readonly IDbConfig _DbConfig;
        private readonly ITracerAuthorisationWriter _Writer;

        public GenerateAuthorisations(IDbConfig dbConfig, ITracerAuthorisationWriter writer)
        {
            _DbConfig = dbConfig;
            _Writer = writer;
        }

        public void Execute(int pAuthorise, Random r)
        {
            using var c = new CosmosClientEx(_DbConfig);
            var unauthorised = c.Query<TracerEntity>()
                .Where(x => x.Authorised == false)
                .Select(x => x.AuthorisationToken)
                .ToArray();

            var authorised = unauthorised
                .Where(x => r.Next(100) <= pAuthorise);

            foreach (var i in authorised)
                _Writer.Execute(new TracerAuthorisationArgs { Token = i}).GetAwaiter().GetResult();
        }
    }
}
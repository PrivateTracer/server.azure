﻿using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class HardCodedTracerValidatorConfig : ITracerValidatorConfig
    {
        public int TracerKeyCountMin => 1;
        public int TracerKeyCountMax => 688; //TODO review.
    }
}

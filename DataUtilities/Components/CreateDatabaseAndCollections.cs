﻿using System;
using System.Threading.Tasks;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig;
using PrivateTracer.Server.Azure.Components.Calibration;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.DbEntities;
using PrivateTracer.Server.Azure.Components.RivmAdvice;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class CreateDatabaseAndCollections
    {
        private readonly IDbConfig _DbConfig;

        public CreateDatabaseAndCollections(IDbConfig config)
        {
            _DbConfig = config;
        }

        public async Task Execute()
        {
            using var c = new CosmosClientEx(_DbConfig);
            await c.CreateDatabaseIfNotExistsAsync();
            await c.CreateContainerIfNotExistsAsync<RivmAdviceConfigEntity>();
            await c.CreateContainerIfNotExistsAsync<RssiThresholdConfigEntity>();

            await c.CreateContainerIfNotExistsAsync<ConfigEntity>();
            await c.CreateContainerIfNotExistsAsync<TracerEntity>();
            await c.CreateContainerIfNotExistsAsync<TracingSetConfigEntity>();

            //TODO await c.CreateContainerIfNotExistsAsync<AgConfigEntity>();
            await c.CreateContainerIfNotExistsAsync<AgTracerEntity>();
            await c.CreateContainerIfNotExistsAsync<AgTracingSetConfigEntity>();
            await c.CreateContainerIfNotExistsAsync<AgTracingCalcConfigEntity>();

            await AddSamples();
        }

        private async Task AddSamples() 
        {
            using (var client = new CosmosClientEx(_DbConfig))
            {
                var e0 = new RivmAdviceConfigEntity
                {
                    Release = new DateTime(2020, 1, 1),
                    Text = new[]
                    {
                        new LocalizableText
                        {
                            Locale = "en-GB", IsolationAdviceShort = "1st", IsolationAdviceLong = "First",
                        },
                        new LocalizableText
                        {
                            Locale = "nl-nl", IsolationAdviceShort = "1e", IsolationAdviceLong = "Eerste",
                        }
                    }
                };
                await client.InsertAsync(e0);

                var e1 = new RivmAdviceConfigEntity
                {
                    Release = new DateTime(2020, 5, 1),
                    IsolationPeriodDays = 10,
                    ObservedTracingKeyRetentionDays = 14,
                    TracingKeyRetentionDays = 15,
                    Text = new[]
                    {
                        new LocalizableText
                        {
                            Locale = "en-GB", IsolationAdviceShort = "Stay the hell indoors for {0} day!!!", IsolationAdviceLong = "Something hmtl, zipped",
                        },
                        new LocalizableText
                        {
                            Locale = "nl-nl", IsolationAdviceShort = "Verklaar de hel binnenshuis", IsolationAdviceLong = "Verklaar de hel binnenshuis but longer.",
                        }
                    }
                };
                await client.InsertAsync(e1);

                var e2 = new RivmAdviceConfigEntity
                {
                    Release = new DateTime(2021, 1, 1),
                };
                await client.InsertAsync(e2);

                var e3 = new RssiThresholdConfigEntity
                {
                    Release = new DateTime(2020, 5, 1),
                    Devices = new[]
                    {
                        new DeviceValue
                        {
                            Id = "Some generic Android", Value = -345.5
                        },
                        new DeviceValue
                        {
                            Id = "Some expensive white phone", Value = -25.5
                        },
                    }
                };
                await client.InsertAsync(e3);

                //TODO something more realistic
                var e4 = new AgTracingCalcConfigEntity
                {
                    Release = new DateTime(2020, 5, 1),
                    MinimumRiskScore = 4,
                    Attenuation = new Weighting { Weight = 30, LevelValues = new []{1} },
                    DaysSinceLastExposure = new Weighting { Weight = 40, LevelValues = new[] { 1,2,3,4 } },
                    DurationLevelValues = new Weighting { Weight = 50, LevelValues = new[] { 10,2,3,4,5 } },
                    TransmissionRisk = new Weighting { Weight = 60, LevelValues = new[] { 10,100,1000 } },
                };
                await client.InsertAsync(e4);
            }
        }
    }
}

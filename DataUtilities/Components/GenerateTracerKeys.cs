﻿using System;
using PrivateTracer.Server.Azure.Components.Services;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class GenerateTracerKeys
    {
        private readonly LuhnModNGenerator _Generator;

        public GenerateTracerKeys(ILuhnModNConfig config)
        {
            _Generator = new LuhnModNGenerator(config);
        }

        public string Next(Func<int, int> random) => _Generator.Next(random);
    }
}
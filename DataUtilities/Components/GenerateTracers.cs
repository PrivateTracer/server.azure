﻿using System;
using System.Collections.Generic;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class GenerateTracers
    {
        private readonly IDbConfig _DbConfig;

        public GenerateTracers(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public void Execute(int tracerCount, Func<int, int> randomInt, Action<byte[]> randomBytes)
        {
            var keyBuffer = new byte[256 / 8];
            var luhnModNConfig = new LuhnModNConfig();
            var tracerKeyGenerator = new GenerateTracerKeys(luhnModNConfig);
            var tracerValidatorConfig = new HardCodedTracerValidatorConfig();
            var tracerKeyValidatorConfig = new HardCodedTracerKeyValidatorConfig();
            var writer = new TracerDbInsertCommand(_DbConfig, new StandardUtcDateTimeProvider());
            var c = new HttpPostTracerCommandV2(
                writer,
                new TracerValidator(
                    tracerValidatorConfig,
                    new TracerAuthorisationTokenLuhnModNValidator(luhnModNConfig),
                        new TracerKeyValidator(tracerKeyValidatorConfig)));

            for (var i = 0; i < tracerCount; i++)
            {
                var tracer = new TracerArgs
                {
                    Token = tracerKeyGenerator.Next(randomInt)
                };

                var keyCount = 1 + randomInt(tracerValidatorConfig.TracerKeyCountMax - 1);
                var keys = new List<TracerKeyArgs>(keyCount);
                for (var j = 0; j < keyCount; j++)
                {
                    randomBytes(keyBuffer);
                    keys.Add(new TracerKeyArgs
                    {
                        Seed = Convert.ToBase64String(keyBuffer),
                        Epoch = tracerKeyValidatorConfig.EpochMin + j
                    });
                }
                tracer.Items = keys.ToArray();

                c.Execute(tracer).GetAwaiter().GetResult();

                if (i > 0 && i % 100 == 0)
                    Console.WriteLine($"Tracer count {i}...");
            }
            Console.WriteLine("Generating tracers finished.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class GenerateAgTracers
    {
        private readonly IDbConfig _DbConfig;

        public GenerateAgTracers(IDbConfig dbConfig)
        {
            _DbConfig = dbConfig;
        }

        public void Execute(int tracerCount, Func<int, int> randomInt, Action<byte[]> randomBytes)
        {
            var luhnModNConfig = new LuhnModNConfig();
            var tracerKeyGenerator = new GenerateTracerKeys(luhnModNConfig);
            var tracerValidatorConfig = new HardCodedAgTracerValidatorConfig();
            var tracerKeyValidatorConfig = new HardCodedAgTracerKeyValidatorConfig();
            var writer = new AgTracerDbInsertCommand(_DbConfig);
            var c = new HttpPostAgTracerCommand(
                writer,
                new AgTracerValidator(
                    tracerValidatorConfig,
                    new TracerAuthorisationTokenLuhnModNValidator(luhnModNConfig),
                        new AgTracerKeyValidator(tracerKeyValidatorConfig)));

            var keyBuffer = new byte[tracerKeyValidatorConfig.DailyKeyByteCount];


            for (var i = 0; i < tracerCount; i++)
            {
                var tracer = new AgTracerArgs
                {
                    Token = tracerKeyGenerator.Next(randomInt)
                };

                var keyCount = 1 + randomInt(tracerValidatorConfig.TracerKeyCountMax - 1);
                var keys = new List<AgTracerKeyArgs>(keyCount);
                for (var j = 0; j < keyCount; j++)
                {
                    randomBytes(keyBuffer);
                    keys.Add(new AgTracerKeyArgs
                    {
                        DailyKey = Convert.ToBase64String(keyBuffer),
                        RollingStart = tracerKeyValidatorConfig.RollingPeriodMin + j,
                        RollingPeriod = 11,
                        Risk = 2
                    });
                }
                tracer.Items = keys.ToArray();

                c.Execute(tracer);

                if (i > 0 && i % 100 == 0)
                    Console.WriteLine($"Tracer count {i}...");
            }
            Console.WriteLine("Generating tracers finished.");
        }
    }
}

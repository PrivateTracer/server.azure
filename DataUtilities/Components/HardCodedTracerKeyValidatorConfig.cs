﻿using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Components
{
    public class HardCodedAgTracerValidatorConfig : IAgTracerValidatorConfig
    {
        public int TracerKeyCountMin => 1;
        public int TracerKeyCountMax => 21;
    }

    public class HardCodedAgTracerKeyValidatorConfig : IAgTracerKeyValidatorConfig
    {
        public int RollingPeriodMin => 1;
        public int RollingPeriodMax => int.MaxValue;
        public int DailyKeyByteCount => 32; //TODO change to real value and use this setting in generator
    }

    /// <summary>
    /// TODO read settings
    /// </summary>
    public class HardCodedTracerKeyValidatorConfig : ITracerKeyValidatorConfig
    {
        public int EpochMin => 1;
        public int EpochMax => int.MaxValue;
        public int KeyByteCount => 32;
    }
}
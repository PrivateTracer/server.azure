﻿using System;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;
using PrivateTracer.Server.Azure.DevServer.DataUtilities.Components;

namespace PrivateTracer.Server.Azure.DevServer.DataUtilities.Authorise
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var pAuthorise = Convert.ToInt32(args[0]);

                if (0 >= pAuthorise || pAuthorise > 100)
                    throw new ArgumentException(nameof(pAuthorise));

                var seed = Convert.ToInt32(args[1]);

                var dbConfig = AppSettingsFromJsonFiles.CreateDbConfig();
                var authorise = new GenerateAuthorisations(dbConfig, new TracerDbAuthoriseCommand(dbConfig));
                authorise.Execute(pAuthorise, new Random(seed));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrivateTracer.Server.Azure.Components.Services;

namespace Components.Tests.Services
{
    [TestClass()]
    public class StandardUtcDateTimeProviderTests
    {
        [TestMethod()]
        public void NowTest()
        {
            Assert.IsTrue(new StandardUtcDateTimeProvider().Now().Kind == DateTimeKind.Utc);
        }
    }
}
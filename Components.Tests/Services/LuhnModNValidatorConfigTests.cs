﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrivateTracer.Server.Azure.Components.Services;

namespace Components.Tests.Services
{
    [TestClass()]
    public class LuhnModNValidatorConfigTests
    {
        [TestMethod()]
        public void GoodDefaults()
        {
            new LuhnModNConfig();
        }

        [TestMethod()]
        public void GoodCustomValues()
        {
            var argle = new LuhnModNConfig("abcdef", 3);
            Assert.AreEqual(3, argle.ValueLength);
            Assert.AreEqual(6, argle.CharacterSet.Length);
            Assert.IsTrue(argle.CharacterSet.Contains('c'));
            Assert.IsTrue(!argle.CharacterSet.Contains('g'));
        }

        [TestMethod()]
        public void BadLengthLo()
        {
            Assert.ThrowsException<ArgumentException>(() => new LuhnModNConfig("abcdef", 1));
        }

        [TestMethod()]
        public void BadCharacterSet()
        {
            Assert.ThrowsException<ArgumentException>(() => new LuhnModNConfig("sdssdfsdf", 2));
        }
    }
}
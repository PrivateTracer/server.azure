using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrivateTracer.Server.Azure.Components.Services;

namespace Components.Tests.Services
{
    [TestClass()]
    public class StringZipTests
    {
        [TestMethod()]
        public void RoundTrip()
        {
            var value = "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. " +
                        "The quick brown fox jumps over the lazy dog. ";

            var valueBytes = Encoding.UTF8.GetBytes(value);
            var zipped = valueBytes.Zip();
            var unzipped = zipped.Unzip();
            CollectionAssert.AreEqual(valueBytes, unzipped);
            var actual = Encoding.UTF8.GetString(unzipped);
            Assert.AreEqual(value, actual);
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Components.Tests.Commands
{
    [TestClass]
    public class UtilityTests
    {
        [TestMethod]
        public void KeyGenerator()
        {
            Trace.WriteLine(DateTime.UtcNow.ToString("u"));


            var key = new byte[256 / 8];
            var r = new Random();
            for (var i = 0; i < 1000; i++)
            {
                r.NextBytes(key);
                var keyString = Convert.ToBase64String(key);
                Trace.WriteLine(string.Join(",", key.Select(x => x)));
                Trace.WriteLine(keyString);
                Trace.WriteLine(keyString.Length);
            }
        }
    }
}
﻿//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using PrivateTracer.Server.Azure.Components.Commands;
//using PrivateTracer.Server.Azure.Components.Entities;
//using PrivateTracer.Server.Azure.Components.Services;

//namespace Components.Tests.Commands
//{
//    [TestClass]
//    public class TracerLifecycleTests
//    {
//        //Very badly designed test that cannot run in parallel with another developer or itself
//        //NB Does not appear to be supported by the CosmosDb Emulator
//        [TestMethod]
//        public void StartHere()
//        {
//            var tracerSubmitted = new Tracer
//            {
//                Token = "12345",
//                Items = new[] {
//                    new TracerKey
//                    {
//                        Seed = "kTIseJvN3Ro2mNDgPnxdOewOzBwzWmXGbg8FEKPNPG0=",
//                        Epoch = 56446554
//                    },
//                    new TracerKey
//                    {
//                        Seed = "voKfxC9vmfhnAMSHDa+EL1C6onAwkHtJDQ2KYcKP78Y=",
//                        Epoch = 98733
//                    },
//                }
//            };

//            var localTestConfigProvider = new McsTestConfigProvider();
//            var standardUtcDateTimeProvider = new StandardUtcDateTimeProvider();

//            var newTracer = new TracerCreateCommand(localTestConfigProvider, standardUtcDateTimeProvider).Execute(tracerSubmitted);
//            Assert.AreEqual(false, newTracer.Authorised);
//            Assert.AreEqual(2, newTracer.TracerKeys.Length);
//            Assert.AreEqual("kTIseJvN3Ro2mNDgPnxdOewOzBwzWmXGbg8FEKPNPG0=", newTracer.TracerKeys.Single(x => x.Epoch == 56446554).Seed);

//            var authorisedTracer = new TracerAuthoriseCommand(localTestConfigProvider).Execute(tracerSubmitted.Token);
//            Assert.AreEqual(true, authorisedTracer.Authorised);

//            var snapshot = new TracerGetAuthorisedSnapshotCommand(localTestConfigProvider).Execute();
//            Assert.AreEqual(true, snapshot.Any());

//            new TracerDeleteSnapshotCommand(localTestConfigProvider).Execute(snapshot);

//            //Test for deletion?
//            var theMissing = new TracerGetAuthorisedSnapshotCommand(localTestConfigProvider).Execute();
//            Assert.AreEqual(false, theMissing.Any());
//        }
//    }

//}
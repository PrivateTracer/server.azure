﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using PrivateTracer.Server.Azure.Components.Commands;
//using PrivateTracer.Server.Azure.Components.Entities;
//using PrivateTracer.Server.Azure.Components.Services;

//namespace Components.Tests.Commands
//{
//    [TestClass]
//    public class TracingSetLifecycleTests
//    {
//        [TestMethod]
//        public void StartHere()
//        {
//            var cuckooFilter = "Cuckoo!Cuckoo!Cuckoo!Cuckoo!Cuckoo!Sparrow!";

//            var localTestConfigProvider = new McsTestConfigProvider();
//            var standardUtcDateTimeProvider = new StandardUtcDateTimeProvider();

//            var ts1 = new TracingSetCreateCommand(localTestConfigProvider, standardUtcDateTimeProvider).Execute(cuckooFilter);
//            var ts2 = new TracingSetCreateCommand(localTestConfigProvider, standardUtcDateTimeProvider).Execute(cuckooFilter);
//            Assert.AreEqual(ts2.Result.TracingSetId, ts1.Result.TracingSetId + 1);

//            var latest = new TracingSetGetLastIdCommand(localTestConfigProvider).Execute();
//            Assert.AreEqual(ts2.Result.TracingSetId, latest);
//        }
//    }
//}
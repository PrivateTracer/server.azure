﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrivateTracer.Server.Azure.Components.TracingSets;
using ProtoBuf;

namespace Components.Tests.Protobuf
{
    [TestClass]
    public class ProtobufTests
    {
        [TestMethod]
        public void Roundtrip()
        {

            var original = new TracingSetGetResponseV2 {Content = "Argle"};
            var mem = new MemoryStream();
            Serializer.SerializeWithLengthPrefix(mem, original, PrefixStyle.Fixed32);
            var bytes = mem.ToArray();

            mem = new MemoryStream(bytes);
            var result = Serializer.DeserializeWithLengthPrefix<TracingSetGetResponseV2>(mem, PrefixStyle.Fixed32);

            Assert.AreEqual(original.Content, result.Content);

        }
    }
}
﻿namespace Components.Tests.CuckooFilters
{
    public class CuckooFilter
    {
        public int[] Values { get; set; }
        public int Length { get; set; }
    }
}
﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrivateTracer.Server.Azure.Components.CuckooFilters;

namespace Components.Tests.CuckooFilters
{
    [TestClass]
    public class CuckooFilterBatchBuilderTests
    {
        [TestMethod]
        public void Dispose()
        {
            var argle = new CuckooFilterBatchBuilder();
            //Compiler error Assert.ThrowsException<ArgumentNullException>(() => argle.Build(null));
            argle.Dispose();
            argle.Dispose(); //2nd one no side effects
            Assert.ThrowsException<ObjectDisposedException>(() => argle.Build(new CfTracerKeyArgs[0]).ToArray());
        }

        [TestMethod]
        public void BuildTest()
        {
            using var cb = new CuckooFilterBatchBuilder();

            var results = cb.Build(TracerKeyArgFeeder.FeedIt(2000000).ToArray());
            
            Assert.AreEqual(2, results.Count());
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrivateTracer.Server.Azure.Components.CuckooFilters;

namespace Components.Tests.CuckooFilters
{
    [TestClass]
    public class CuckooFilterBuilderTests
    {
        [TestMethod]
        public void CuckooFilterBuilderTest()
        {
            new CuckooFilterBuilder().Dispose();
        }

        [TestMethod]
        public void TryAddTest()
        {
            using var cb = new CuckooFilterBuilder();
            cb.TryAdd(new CfTracerKeyArgs
            {
                Seed = "Eak9wlNggR+YVFDqgS/iM0SnsR03XgzrMwIFaj0LyTY=",
                Epoch = 524235
            });
        }

        [TestMethod]
        public void BuildTest()
        {
            using var cb = new CuckooFilterBuilder();
            Assert.AreEqual(false, cb.HasContent);
            cb.TryAdd(new CfTracerKeyArgs
            {
                Seed = "Eak9wlNggR+YVFDqgS/iM0SnsR03XgzrMwIFaj0LyTY=",
                Epoch = 524235
            });
            Assert.AreEqual(true, cb.HasContent);
            var resultString = cb.Build();

            Assert.IsTrue(!string.IsNullOrWhiteSpace(resultString));

            //var result = JsonConvert.DeserializeObject<CuckooFilter>(resultString);

            //var distinctCounts = result.Values
            //    .GroupBy(x => x)
            //    .ToDictionary(x => x.Key, x => x.Count());

            //Assert.AreEqual(1, distinctCounts[82]);
            //Assert.AreEqual(1048575, distinctCounts[100]);
            //Assert.AreEqual(result.Values.Length, distinctCounts[82]+ distinctCounts[100]);
            //Assert.AreEqual(1, result.Length);

            //Trace.WriteLine($"Entries: {1}, Size: {resultString.Length}, Zip: {Encoding.UTF8.GetBytes(resultString).Zip().Length}");
        }

        [TestMethod]
        public void FillTest()
        {
            using var cb = new CuckooFilterBuilder();
            Assert.AreEqual(false, cb.HasContent);

            var key = new byte[256 / 8];
            var r = new Random();
            var counter = 0;
            r.NextBytes(key);
            while(!cb.Full)
            {
                r.NextBytes(key);
                cb.TryAdd(new CfTracerKeyArgs
                {
                    Seed = Convert.ToBase64String(key),
                    Epoch = 10000+ r.Next(100) //TODO open to suggestions for something closer to reality
                });
                counter++;
            }

            Assert.AreEqual(true, cb.HasContent);
            var resultString = cb.Build();
            //var result = JsonConvert.DeserializeObject<CuckooFilter>(resultString);
            //Entries: 1007003, Size: 3762407, Zip: 1402354 => 3Mb 1.4Mb approx half size
            //Entries: 1008033, Size: 3761361, Zip: 1402445
            //Trace.WriteLine($"Entries: {counter}, Size: {resultString.Length}, Zip: { Encoding.UTF8.GetBytes(resultString).Zip().Length}");
        }

        [TestMethod]
        public void FillTestRoomHeater()
        {
            Parallel.For(0, 10, _ => FillTest());
            //Entries: 1005848, Size: 3762125, Zip: 1402595
            //Entries: 1007921, Size: 3760466, Zip: 1402133
            //Entries: 1004428, Size: 3762737, Zip: 1403043
            //Entries: 1007182, Size: 3760792, Zip: 1402365
            //Entries: 1010572, Size: 3760634, Zip: 1401702
            //Entries: 1007287, Size: 3761838, Zip: 1402276
            //Entries: 1001596, Size: 3763559, Zip: 1403421
            //Entries: 1009189, Size: 3761252, Zip: 1402384
            //Entries: 1008086, Size: 3759631, Zip: 1402615
            //Entries: 1004727, Size: 3762993, Zip: 1403078

        }
    }
}
﻿using System;
using System.Collections.Generic;
using PrivateTracer.Server.Azure.Components.CuckooFilters;

namespace Components.Tests.CuckooFilters
{
    public static class TracerKeyArgFeeder
    {
        public static IEnumerable<CfTracerKeyArgs> FeedIt(int count)
        {
            var r = new Random();
            var key = new byte[256 / 8];
            for (var i = 0; i < count; i++)
            {
                r.NextBytes(key);
                yield return new CfTracerKeyArgs
                {
                    Seed = Convert.ToBase64String(key),
                    Epoch = 10000 + r.Next(100) //TODO open to suggestions for something closer to reality
                };
            }
        }
    }
}
# Server.Azure

NB. For the technical design and archtectute, go to the Documentation folder.

Single solution containing the .NET Core server application components.

The production server components are Azure Function Apps while local development support is provided by ServerLite, an .NET Core MVC Web App, and the command line Data Utilites.

## Development Tools

* Visual Studio 2019 Community
* Microsoft Azure Storage Explorer
* Microsoft Azure CosmosDB Emulator or Azure CosmosDB account. Project members can request access to a personal dev instance of the db if they have issues with the Cosmos DB Emulator.

## Supporting local mobile app development

1. Download dotnet SDK: https://dotnet.microsoft.com/download/dotnet-core/3.1
1. Clone this repo
1. Choose your database - local emulator or Azure account
1. Add an `appsettings.Development.json` file. This overrides the settings in appsettings.json. Leave it empty if if using the local Cosmos DB emulator (the compiler expects this file to be present but it is deliberately excluded as it contains secrets), otherwise add the Uri, Token and Id settings you are given by the Server Admins.
1. Go to the ProvisionDb folder and run it with 'dotnet run'.
1. Run gentracers, genauth, gentracingsets as required.
1. Go to the ServerLite folder and run it with 'dotnet run'.

See the individual app folders for details.
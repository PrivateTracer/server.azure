# Introduction

This is a working document intended to capture technical design decisions for the implementation. The reader is assumed to have read the project overview from README.md in the Coordination project. 
Extensive use is made of the [Archimate](https://www.opengroup.org/archimate-forum/archimate-overview) entrerprise architecture modelling language in this document.

How the application is used in detail is covered in the documentation in the Coordination project.

The focus of this document are the services and mechanisms to support the primary function of the mobile application by the storing, processing and distribution of anonymised exposure data.

![Archimate Business View](Images/BusinessView2.png "High-level Archimate Business View")

From the above diagram:

* __Tracing Set__ - An anonymised set of Tracing Keys, to enable the clients to detect whether the user has been in proximity with an infected person. Currently implemented as a [cuckoo filter](https://en.wikipedia.org/wiki/Cuckoo_filter).
* __Tracer__ - A collection of exposure keys held in escrow on the server and an authorisation code to effect its release.

The key server side responsibilities include:

* __Escrow storage of Tracers__ - Storing the information related to the known set of infected interactions and subsequently releasing the information when authorised or disposing of it, as soon as possible, if not.
* __Tracing Set preparation and distribution__ - Aggregates all the infected Tracer Keys released from escrow for sharing with clients in an effective manner.
* __Configuration Management__ - Stores and distributes configuration parameters for the mobile application e.g. RIVM advice.

# Server Application Architecture

The server application architecture consists of a set of simple, discrete, single-purpose services hosted in Microsoft Azure.
Each is deployed following the least-required-access principle, presenting the minimum of attack surface to clients or other application components, using private networks where appropriate.
Components are deployed in multiple locations with multiple instances.
This is intended to add strength in depth and limit the scope of any attack and provide redundancy to mitigate against failures or outages in the system.

![Applications View](Images/ApplicationsView.png "Applications View")

# Application and Technology

The server-side components are all hosted on Microsoft Azure to leverage the security features inherent from that platform.

All components are deployed as Function Apps, using a Service Plan dependent on the degree of redundancy required. 
Tracer Receiver alone requires the ability to scale out quickly and be tolerant of outage within an Azure Region. 
This will use the App Service plan with multiple Availability Zones. The remainder will simply use Basic or Premium as necessary. 
Any given application will be deployed in an Active-Passive fail-over configuration in 2 different Azure Regions.

Communication between application computational components uses Azure Service Bus.

The Tracer escrow storage will be a geo-redundant instance of Cosmos DB, using 2 Azure Regions. Local data storage will also use Cosmos DB.

Data at rest will be encrypted in both the message queues and the databases where appropriate.

The applications use the proven .NET Core stack and our own implementation of the DP3T algorithms in Rust.

![Overview](Images/Overview.png "Overview")

# Deployment Environment Technology

The diagram below shows the specific technology choices from Azure used to achieve the security and redundancy non-functional requirements.

![Deployment and Technology](Images/DeploymentTechnology.png "Deployment and Technology")

# Operations

All access tokens and other secrets use Azure Vault to regularly rotate the values of the secrets and provide access to application configuration in a secure manner.

The production system makes the minimum amount of telemetry available to operations to preserve the integrity of the operation of the system.

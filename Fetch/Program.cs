﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PrivateTracer.Server.Azure.Components.TracingSets;

namespace Fetch
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Enter the cf id:");
            var id = Console.ReadLine();
            Console.WriteLine(await Get($"https://localhost:44357/v2/tracingsets/{id}"));
        }

        private static async Task<string> Get(string url)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/x-protobuf"));

            var httpResult = await httpClient.GetAsync(url);
            var result = ProtoBuf.Serializer.Deserialize<TracingSetGetResponseV2>(await httpResult.Content.ReadAsStreamAsync());
            return JsonConvert.SerializeObject(result);
        }
    }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PrivateTracer.Server.Azure.Components.ag;
using PrivateTracer.Server.Azure.Components.ag.Config;
using PrivateTracer.Server.Azure.Components.ag.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.ag.Tracers;
using PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig;
using PrivateTracer.Server.Azure.Components.ag.TracingSets;
using PrivateTracer.Server.Azure.Components.Calibration;
using PrivateTracer.Server.Azure.Components.Config;
using PrivateTracer.Server.Azure.Components.RivmAdvice;
using PrivateTracer.Server.Azure.Components.Services;
using PrivateTracer.Server.Azure.Components.Settings;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.Tracers;
using PrivateTracer.Server.Azure.Components.TracingSets;
using PrivateTracer.Server.Azure.ServerLite.Components;
using WebApiContrib.Core.Formatter.Protobuf;

namespace PrivateTracer.Server.Azure.ServerLite
{
    public class Startup
    {
        public Startup(IHostEnvironment env)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.RespectBrowserAcceptHeader = true;
            }).AddProtobufFormatters();

            services.AddSingleton<IUtcDateTimeProvider, StandardUtcDateTimeProvider>();
            
            services.AddSingleton<ILuhnModNConfig, LuhnModNConfig>();
            services.AddSingleton<IDbConfig>(new DbConfigAppSettings(Configuration, DatabaseConfigNames.Main));
            services.AddSingleton<IDp3tConfig, Dp3tAppSettings>();
            services.AddSingleton<IAgConfig, AgConfigAppSettings>();

            services.AddSingleton<ITracerValidator, TracerValidator>();
            services.AddSingleton<ITracerValidatorConfig, TracerValidationAppSettings>();
            services.AddSingleton<ITracerKeyValidator, TracerKeyValidator>();
            services.AddSingleton<ITracerKeyValidatorConfig, TracerKeyValidatorAppSettings>();

            services.AddSingleton<ITracerAuthorisationTokenValidator, TracerAuthorisationTokenLuhnModNValidator>();

            ///PT
            services.AddScoped<HttpPostTracerCommandV2, HttpPostTracerCommandV2>();
            services.AddScoped<ITracerWriter, TracerDbInsertCommand>();
            
            services.AddScoped<HttpPostTracerAuthoriseCommand, HttpPostTracerAuthoriseCommand>();
            services.AddScoped<ITracerAuthorisationWriter, TracerDbAuthoriseCommand>();

            services.AddScoped<HttpGetLatestConfigCommand, HttpGetLatestConfigCommand>();
            services.AddScoped<ConfigBuilder, ConfigBuilder>();
            services.AddScoped<TracingSetsConfigBuilder, TracingSetsConfigBuilder>();
            services.AddScoped<GetLatestRivmAdviceCommand, GetLatestRivmAdviceCommand>();
            services.AddScoped<GetLatestRssiThresholdsCommand, GetLatestRssiThresholdsCommand>();

            services.AddScoped<HttpGetTracingSetCommandV2, HttpGetTracingSetCommandV2>();
            services.AddScoped<TracingSetSafeReadCommand, TracingSetSafeReadCommand>();

            services.AddScoped<HttpGetRivmAdviceCommand, HttpGetRivmAdviceCommand>();
            services.AddScoped<SafeGetRivmAdviceCommand, SafeGetRivmAdviceCommand>();

            services.AddScoped<HttpPostRivmAdviceCommand, HttpPostRivmAdviceCommand>();
            services.AddScoped<RivmAdviceInsertDbCommand, RivmAdviceInsertDbCommand>();
            services.AddScoped<RivmAdviceValidator, RivmAdviceValidator>();

            services.AddScoped<HttpPostRssiThresholdCommand, HttpPostRssiThresholdCommand>();
            services.AddScoped<RssiThresholdInsertDbCommand, RssiThresholdInsertDbCommand>();
            services.AddScoped<RssiThresholdValidator, RssiThresholdValidator>();

            services.AddScoped<HttpPostTracerCommandV2, HttpPostTracerCommandV2>();
            
            services.AddScoped<HttpGetRssiThresholdsCommand, HttpGetRssiThresholdsCommand>();
            services.AddScoped<SafeGetRssiThresholdsCommand, SafeGetRssiThresholdsCommand>();


            ///AG
            services.AddSingleton<IAgTracerValidator, AgTracerValidator>();
            services.AddSingleton<IAgTracerValidatorConfig, AgTracerValidationAppSettings>();
            services.AddSingleton<IAgTracerKeyValidator, AgTracerKeyValidator>();
            services.AddSingleton<IAgTracerKeyValidatorConfig, AgTracerKeyValidatorAppSettingsConfig>();

            services.AddScoped<HttpPostAgTracerCommand, HttpPostAgTracerCommand>();
            services.AddScoped<IAgTracerWriter, AgTracerDbInsertCommand>();

            //TODO services.AddScoped<HttpPostAgTracerAuthoriseCommand, HttpPostAgTracerAuthoriseCommand>();
            //TODO services.AddScoped<ITracerAuthorisationWriter, AgTracerDbAuthoriseCommand>();

            services.AddScoped<HttpGetLatestAgConfigCommand, HttpGetLatestAgConfigCommand>();
            services.AddScoped<AgConfigBuilder, AgConfigBuilder>();
            services.AddScoped<AgTracingSetsConfigBuilder, AgTracingSetsConfigBuilder>();
            services.AddScoped<GetLatestTracingCalcConfigCommand, GetLatestTracingCalcConfigCommand>();

            services.AddScoped<HttpGetAgTracingSetCommand, HttpGetAgTracingSetCommand>();
            services.AddScoped<AgTracingSetSafeReadCommand, AgTracingSetSafeReadCommand>();

            services.AddScoped<HttpPostAgTracerCommand, HttpPostAgTracerCommand>();

            services.AddScoped<HttpGetAgTracingSetCommand, HttpGetAgTracingSetCommand>();

            services.AddScoped<HttpGetTracingCalcConfigCommand, HttpGetTracingCalcConfigCommand>();
            services.AddScoped<SafeGetTracingCalcConfigCommand, SafeGetTracingCalcConfigCommand>();

            services.AddScoped<HttpPostAgTracerAuthoriseCommand, HttpPostAgTracerAuthoriseCommand>();
            services.AddScoped<IAgTracerAuthorisationWriter, AgTracerDbAuthoriseCommand>();


            services.AddScoped<HttpPostTracingCalcConfigCommand, HttpPostTracingCalcConfigCommand>();
            services.AddScoped<TracingCalcConfigInsertDbCommand, TracingCalcConfigInsertDbCommand>();
            services.AddScoped<TracingCalcConfigValidator, TracingCalcConfigValidator>();

            services.AddSwaggerGen(o =>
            {
                o.SwaggerDoc("v1", new OpenApiInfo { Title = "Private Tracer Stand-Alone Development Server", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(o =>
                {
                    o.SwaggerEndpoint("/swagger/v1/swagger.json", "Private Tracer Stand-Alone Development Server V1");
                });
            //}

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}

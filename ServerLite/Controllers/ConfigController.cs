﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.Config;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConfigController : ControllerBase
    {
        [HttpGet]
        [Route("/config")]
        public IActionResult GetLatestConfig([FromServices]HttpGetLatestConfigCommand command)
        {
            return command.Execute();
        }
    }
}

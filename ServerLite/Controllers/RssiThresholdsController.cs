﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.Calibration;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RssiThresholdsController : ControllerBase
    {
        [HttpGet]
        [Route("/rssithresholds/{id}")]
        public IActionResult GetById(string id, [FromServices]HttpGetRssiThresholdsCommand command)
        {
            return command.Execute(id);
        }

        [HttpPost]
        [Route("/rssithresholds")]
        public IActionResult Post([FromBody]RssiThresholdArgs args, [FromServices]HttpPostRssiThresholdCommand command)
        {
            return command.Execute(args);
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.Calibration;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgRssiThresholdsController : ControllerBase
    {
        [HttpGet]
        [Route("/ag/rssithresholds/{id}")]
        public IActionResult GetById(string id, [FromServices]HttpGetRssiThresholdsCommand command)
        {
            return command.Execute(id);
        }

        [HttpPost]
        [Route("/ag/rssithresholds")]
        public IActionResult Post([FromBody]RssiThresholdArgs args, [FromServices]HttpPostRssiThresholdCommand command)
        {
            return command.Execute(args);
        }
    }
}
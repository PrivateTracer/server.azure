﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.ag.Config;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgConfigController : ControllerBase
    {
        [HttpGet]
        [Route("/ag/config")]
        public IActionResult GetLatestConfig([FromServices]HttpGetLatestAgConfigCommand command)
        {
            return command.Execute();
        }
    }
}

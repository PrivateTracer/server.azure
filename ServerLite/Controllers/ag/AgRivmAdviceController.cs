﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.RivmAdvice;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgRivmAdviceController : ControllerBase
    {
        [HttpGet]
        [Route("/ag/rivmadvice/{id}")]
        public IActionResult GetById(string id, [FromServices]HttpGetRivmAdviceCommand command)
        {
            return command.Execute(id);
        }

        [HttpPost]
        [Route("/ag/rivmadvice")]
        public IActionResult Post([FromBody]MobileDeviceRivmAdviceArgs args, [FromServices]HttpPostRivmAdviceCommand command)
        {
            return command.Execute(args);
        }
    }
}
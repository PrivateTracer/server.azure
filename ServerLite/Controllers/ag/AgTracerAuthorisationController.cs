﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.ag.TracerAuthorisation;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgTracerAuthorisationController : ControllerBase
    {
        /// <summary>
        /// A Tracer is Activated.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("/ag/authorise")]
        public IActionResult PostActivateTracingKey([FromBody]TracerAuthorisationArgs args, [FromServices]HttpPostAgTracerAuthoriseCommand command)
        {
            return command.Execute(args);
        }
    }
}

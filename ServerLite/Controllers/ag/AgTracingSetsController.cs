﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.ag.TracingSets;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgTracingSetsController : ControllerBase
    {
        [HttpGet]
        [Route("/ag/tracingsets/{id}")]
        public IActionResult GetTracingSets(string id, [FromServices]HttpGetAgTracingSetCommand command)
        {
            return command.Execute(id);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.ag.Tracers;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgTracerController : ControllerBase
    {
        [HttpPost]
        [Route("/ag/tracer")]
        public IActionResult PostTracer([FromBody]AgTracerArgs args, [FromServices]HttpPostAgTracerCommand command)
        {
            return command.Execute(args);
        }
    }
}
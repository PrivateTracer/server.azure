﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.ag.TracingCalcConfig;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers.ag
{
    [ApiController]
    [Route("[controller]")]
    public class AgTracingCalcConfigController : ControllerBase
    {
        [HttpGet]
        [Route("/ag/tracingcalcconfig/{id}")]
        public IActionResult GetLatestConfig(string id, [FromServices]HttpGetTracingCalcConfigCommand command)
        {
            return command.Execute(id);
        }

        [HttpPost]
        [Route("/ag/tracingcalcconfig")]
        public async Task<IActionResult> Post([FromBody]TracingCalcConfigArgs args, [FromServices]HttpPostTracingCalcConfigCommand command)
        {
            return await command.Execute(args);
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.TracingSets;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TracingSetsController : ControllerBase
    {
        //[HttpGet]
        //[Route("/tracingsets/{id}")]
        //public IActionResult GetTracingSets(string id, [FromServices]HttpGetTracingSetCommand command)
        //{
        //    return command.Execute(id);
        //}

        [HttpGet]
        [Route("/tracingsets/{id}")]
        public IActionResult GetTracingSetsProtobuf(string id, [FromServices]HttpGetTracingSetCommandV2 command)
        {
            return command.Execute(id);
        }
    }
}

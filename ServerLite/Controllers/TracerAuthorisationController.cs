﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.TracerAuthorisation;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TracerAuthorisationController : ControllerBase
    {
        /// <summary>
        /// A Tracer is Activated.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("/authorise")]
        public IActionResult PostActivateTracingKey([FromBody]TracerAuthorisationArgs args, [FromServices]HttpPostTracerAuthoriseCommand command)
        {
            return command.Execute(args);
        }
    }
}

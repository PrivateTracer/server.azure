﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.Tracers;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TracerControllerV2 : ControllerBase
    {
        [HttpPost]
        [Route("/tracer")]
        public Task<IActionResult> PostTracer([FromBody]TracerArgs args, [FromServices]HttpPostTracerCommandV2 command)
        {
            return command.Execute(args);
        }
    }
}
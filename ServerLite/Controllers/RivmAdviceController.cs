﻿using Microsoft.AspNetCore.Mvc;
using PrivateTracer.Server.Azure.Components.RivmAdvice;

namespace PrivateTracer.Server.Azure.ServerLite.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RivmAdviceController : ControllerBase
    {
        [HttpGet]
        [Route("/rivmadvice/{id}")]
        public IActionResult GetById(string id, [FromServices]HttpGetRivmAdviceCommand command)
        {
            return command.Execute(id);
        }

        [HttpPost]
        [Route("/rivmadvice")]
        public IActionResult Post([FromBody]MobileDeviceRivmAdviceArgs args, [FromServices]HttpPostRivmAdviceCommand command)
        {
            return command.Execute(args);
        }
    }
}
NB. This needs testing!!!!!

# Server Lite

The aim is to allow mobile client developers to run the server endpoints locally and manipulate scenarios and data without Azure.

## Requirements

* .NET Core 3.1

Either:

* The Azure Cosmos DB Emulator - Note this is problematic on Linux

Or

* An Azure Cosmos DB account


## Settings

None for running ServerLite on Windows with the Cosmos DB Emulator.

If using an Azure-hosted CosmosDB, you will need the uri, token and a name for the DB (use your handle?)


## Running the Dev Server

* Clone the repo.
* Start the Cosmos Db Emulator
* Move to the DataUtilties.ProvisionDb folder
* Type 'dotnet run provisiondb ...your new db name here...'
* Move to the ServerLite. folder
* Edit the appsetting.config file and change the Database Id setting to the name you provisioned.
* Type 'dotnet run ServerLite'
